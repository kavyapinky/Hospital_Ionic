var Config = (function(){
  'use strict';
  var cfg = {
    appVersion: '~',
    debug: true, // to toggle features between dev & prod
    verbose: true, // should log in console more infos
    track: false, // should send tracking events to a server
    storage: true, // should save data to browser storage
    storagePrefix: 'app-', // prefix all stoarge entries with this prefix
    emailSupport: 'taalomat.bangalore@gmail.com',
    //backendUrl: 'http://tmtapi.xcdify.com:80/api/',
    backendUrl: 'http://gymapi.xcdify.com/api/',
    parse: {
      applicationId: '4410af2f',
      restApiKey: ''
    },
    gcm: {
      // create project here : https://console.developers.google.com/
      senderID: '65137484242', // Google project number
      apiServerKey: 'AIzaSyDhPeRcH4HJU1sF6y5qfOrSoOsRVcoC9u4' // used only to send notifications
    },
    apiKey : {
      login :'auth/login',
      forgotPassword : 'auth/password/forgetmail',
      changePassword: 'auth/password/change',
      sendmail : 'auth/sendmail',
      categories :'categories',
      contactus : 'contactus',
      organizationStatus: 'provider/organization/status',
      organization : 'provider/organization',
      organizationByUserId: 'provider/organization/user',
      registerDevice : 'register/device',
      sendBatchNotification : 'send/batch-notification',
      sendNotificationByUserId : 'send/users',
      sendNotification : 'PushBotsApi',
      rating: 'rating',
      request: 'request',
      roles : 'roles',
      users : 'users',
      providerUser : 'provider-users'


    }
  };
  return cfg;
})();
