(function(){
  'use strict';
  angular.module('app', ['ionic', 'ngCordova', 'LocalForageModule','pascalprecht.translate','ngAutocomplete','ionic-datepicker','ionic.rating','ngCordovaOauth','ion-google-place' ,'ngResource'])
    .config(configure)
    .run(runBlock);

  configure.$inject = ['$urlRouterProvider', '$provide', '$httpProvider','$cordovaAppRateProvider'];
  function configure($urlRouterProvider, $provide, $httpProvider,$cordovaAppRateProvider){
    // ParseUtilsProvider.initialize(Config.parse.applicationId, Config.parse.restApiKey);

    $urlRouterProvider.otherwise('/main');

    // improve angular logger
    $provide.decorator('$log', ['$delegate', 'customLogger', function($delegate, customLogger){
      return customLogger($delegate);
    }]);

    // configure $http requests according to authentication
    $httpProvider.interceptors.push('AuthInterceptor');


    document.addEventListener("deviceready", function () {

       var prefs = {
         language: 'en',
         appName: 'TaaloMat-Customer',
         iosURL: '4410af2f',
         androidURL: 'market://details?id=com.ionicframework.TaaloMatCustomer4410af2f'
       };

       $cordovaAppRateProvider.setPreferences(prefs)

    }, false);

  }

  function runBlock($rootScope, $state, $log, AuthSrv, UserSrv, PushPlugin, ToastPlugin, Config,$translate,$ionicPlatform,$ionicScrollDelegate,StorageUtils,$ionicLoading){
    checkRouteRights();
    setupPushNotifications();
    userLangTranslater();

    $ionicPlatform.ready(function () {
        $ionicScrollDelegate.scrollTop();
    });


    $rootScope.appOffline = false;
    $rootScope.version = 0.91;
    $rootScope.num_taxi = "8719";
    $rootScope.app_store = "";
    $rootScope.platform = "pc";

    ////////////////



    $rootScope.$on('loading:hide', function() {
      $ionicLoading.hide()
    })

    function checkRouteRights(){
      $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){
        if(toState && toState.data && Array.isArray(toState.data.restrictAccess)){
          var restricted = toState.data.restrictAccess;
          var logged = AuthSrv.isLogged();
          if (AuthSrv.isSkip) {
              $state.go('loading');
              return;
          }

          if(logged && restricted.indexOf('notLogged') > -1){
            event.preventDefault();
            $log.log('IllegalAccess', 'State <'+toState.name+'> is restricted to non logged users !');
            $state.go('loading');
          } else if(!logged && restricted.indexOf('logged') > -1){
            event.preventDefault();
            $log.log('IllegalAccess', 'State <'+toState.name+'> is restricted to logged users !');
            $state.go('loading');
          }
        }
      });
    }

    function setupPushNotifications(){
      // /!\ To use this, you should add Push plugin : ionic plugin add https://github.com/phonegap-build/PushPlugin
      // registrationId should be uploaded to the server, it is required to send push notification
      PushPlugin.register(Config.gcm.senderID).then(function(registrationId){
        return UserSrv.get().then(function(user){
          if(!user){ user = {}; }
          user.pushId = registrationId;

          return UserSrv.set(user);
        });
      });
      PushPlugin.onNotification(function(notification){

        ToastPlugin.show('Notification received: '+notification.payload.message || notification.message);

        StorageUtils.get('notification').then(function(result){
          var data = result || [];
          data.push({message : (notification.payload.message || notification.message) , date : new Date()});
          StorageUtils.set('notification', data);
        });

      });
    }

    function userLangTranslater(){
      return UserSrv.get().then(function(user){
          if(!user){ user = {}; }
            user.lang = 'en';
          $translate.use(user.lang);
          return UserSrv.set(user);
        });
    }

  }
})();
