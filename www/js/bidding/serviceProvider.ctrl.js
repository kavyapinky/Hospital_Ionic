(function(){
  'use strict';
  angular.module('app')
  .controller('ServiceProviderCtrl', ServiceProviderCtrl);

  function ServiceProviderCtrl($scope, $state, $http ,UserSrv, AuthSrv,$ionicPopup,RequestSrv,$stateParams,SubCategoryService){
    var vm = {};
    $scope.vm = vm;
    UserSrv.hideSpinner();
    $scope.venderRating = [];

    $scope.vm.rating= {
      rate :0,
      max :5,
      comments:''
    }
   
    $scope.$on('$ionicView.enter', function(viewInfo){
      getUser();
      getBidding();
      getBidderInfo();
      getAllUserRating($stateParams.userId);
    });

    function getUser(){
      return UserSrv.get().then(function(user){
        vm.user = user;
      });
    };


    function getBidding(){
      RequestSrv.get($stateParams.requestId).then(function (res) {
          $scope.biddingDetails = res.data;
          $scope.biddingDetails.BiddingDetails = JSON.parse(res.data.BiddingDetails);
          $scope.biddingDetails.ChatDetails = JSON.parse(res.data.ChatDetails);

          $scope.BiddingDetails = _.filter($scope.biddingDetails.BiddingDetails,function(data){
            return data.SPId == $stateParams.userId;
          })[0] || [];

          SubCategoryService.get($scope.biddingDetails.CategoryId).then(function(data){
              $scope.biddingDetails.Category= data;
          });

      });
    }

    function getBidderInfo(){
      RequestSrv.getOrganization($stateParams.userId).then(function (res) {
          if (res.data.length>0) {
            $scope.vm.serviceProvider = res.data[0];
          }
      });
    }

    function getAllUserRating(){
      RequestSrv.getAllUserRating($stateParams.userId).then(function (res) {
          $scope.venderRating= res.data
      });
    }


    $scope.goToComments = function (sp) {
        $scope.biddingDetails.selectedSpId=sp.SPId
        $state.go('app.chat', { "request": $scope.biddingDetails });;
    };

    $scope.selectBidder = function (sp) {
      UserSrv.showSpinner();
      if(sp.price == 0){
        var alertPopup = $ionicPopup.alert({
          title: 'Message',
          template: 'Provider Not yet Bidd for this request'
        });
        return;
      }

      $scope.biddingDetails.Status='inProgress';
      $scope.biddingDetails.SPId=sp.SPId;
      $scope.biddingDetails.BiddingDetails = JSON.stringify($scope.biddingDetails.BiddingDetails);
      $scope.biddingDetails.ChatDetails = JSON.stringify($scope.biddingDetails.ChatDetails);
      $http.post(Config.backendUrl +  Config.apiKey.request , JSON.stringify($scope.biddingDetails)).success(function (response) {
        UserSrv.hideSpinner();
        var alertPopup = $ionicPopup.alert({
          title: 'Success',
          template: 'SuccessFully select the Bidder'
        });
        alertPopup.then(function() {
          $state.go('app.category');
        });

      }).error(function (error) {

        UserSrv.hideSpinner();
        vm.error = error.data && error.data.ExceptionMessage ? error.data.ExceptionMessage : "Something went wrong, Please check all credentials and try again";
        var alertPopup = $ionicPopup.alert({
          title: 'Message',
          template: vm.error 
        });

      });
    };

  }
})();
