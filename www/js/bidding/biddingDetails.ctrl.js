(function(){
  'use strict';
  angular.module('app')
  .controller('biddingDetailCtrl', biddingDetailCtrl);

  function biddingDetailCtrl($scope, $state, $stateParams, RequestSrv, SubCategoryService,$http, $ionicPopup,UserSrv,Config){
    var vm = {};
    $scope.vm = vm;
    $scope.vm.isRatungPending = false;
    $scope.biddingDetails= [];
    UserSrv.hideSpinner();

    $scope.$on('$ionicView.enter', function(viewInfo){
      getUser();
      
    });

    function getUser(){
      return UserSrv.get().then(function(user){
        $scope.vm.user = user;
        getUserRating();
        getBidding();
      });
    };

    function getUserRating(){
      RequestSrv.getUserRating($scope.vm.user.Id,$stateParams.requestId).then(function(res){
        if (!res.data) {
          $scope.vm.isRatungPending = true;
        };
      });
    };

    $scope.vm.rating= {
      rate :0,
      max :5,
      comments:''
    }

    function getBidding(){
      UserSrv.showSpinner();
      RequestSrv.get($stateParams.requestId).then(function (res) {
        UserSrv.hideSpinner();
        $scope.biddingDetails = res.data;
        $scope.biddingDetails.BiddingDetails = JSON.parse(res.data.BiddingDetails);
        $scope.biddingDetails.ChatDetails = JSON.parse(res.data.ChatDetails);

        SubCategoryService.get($scope.biddingDetails.CategoryId).then(function(data){
          $scope.biddingDetails.Category= data;
        });
          //CategoryId
        }, function(error){
          UserSrv.hideSpinner();
          vm.error = error.data && error.data.ExceptionMessage ? error.data.ExceptionMessage : "Something went wrong, Please check all credentials and try again";
          var alertPopup = $ionicPopup.alert({
            title: 'Warning',
            template: vm.error 
          });

        });
    }
    
    $scope.goToComments = function (sp) {
      $scope.biddingDetails.selectedSpId=sp.SPId
      $state.go('app.chat', { "request": $scope.biddingDetails });;
    };

    $scope.goToBidderDetails = function (sp) {
      $scope.biddingDetails.selectedSpId=sp.SPId
      $state.go('app.serviceProvider', { "requestId": $stateParams.requestId, "userId": sp.SPId});;
    };

    $scope.selectBidder = function (sp) {

      if(sp.price <= 0)
      {
        var alertPopup = $ionicPopup.alert({
          title: 'Message',
          template: 'Provider Not yet Bidd for this request'
        });
        return;
      }


      $http.post(Config.backendUrl + Config.apiKey.request +'/'+ $stateParams.requestId +'/status?spId='+sp.SPId+'&status=inProgress&userId='+vm.user.Id, {}).success(function (response) {

        var alertPopup = $ionicPopup.alert({
          title: 'Success',
          template: 'SuccessFully selected the Bidder'
        });
        alertPopup.then(function() {
          $state.go('app.category');
        });

      }).error(function (error) {

        UserSrv.hideSpinner();
        vm.error = error.data && error.data.ExceptionMessage ? error.data.ExceptionMessage : "Something went wrong, Please try again";
        var alertPopup = $ionicPopup.alert({
          title: 'Warning',
          template: vm.error 
        });

      });
    };


    
  }

})();






