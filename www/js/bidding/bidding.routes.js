(function(){
  'use strict';
  angular.module('app')
    .config(configure);

  function configure($stateProvider){
    $stateProvider
    .state('app.bidding', {
      url: '/bidding/:requestId',
      cache: false,
      views: {
        'menuContent': {
          templateUrl: 'js/bidding/bidding-details.html',
          controller: 'biddingDetailCtrl'
        }
      }
    })
    .state('app.serviceProvider', {
      url: '/serviceProvider/:requestId/:userId',
      cache: false,
      views: {
        'menuContent': {
          templateUrl: 'js/bidding/serviceProvider.html',
          controller: 'ServiceProviderCtrl'
        }
      }
    });
  }



})();
