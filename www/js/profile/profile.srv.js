(function(){
  'use strict';
  angular.module('app')
    .factory('ProflieSrv', ProflieSrv);

  ProflieSrv.$inject = ['$http', 'UserSrv', 'api','api_config'];
  function ProflieSrv($http, UserSrv, api,apiRoot){
    var service = {
      updateUser : updateUser,
      getUser : getUser,
    };
    return service;

   

    function getUser(userid){
       
      return api.users.getprofile.get({userId:userid}).$promise.then(function(res){
        return res.Result;
      })

    }


    function updateUser(fd){
       
      return $http.post(apiRoot.api_root + '/webapi/users/updateprofile', fd, {
        transformRequest: angular.identity,
        headers: {'Content-Type': undefined}
      })
      .success(function(res){
        return res;
      })
      .error(function(error){
        return error;
      });

    }




  }

})();
