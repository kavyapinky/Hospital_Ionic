(function(){
  'use strict';
  angular.module('app')
    .controller('profileCtrl', profileCtrl);

  function profileCtrl($scope, $state, $ionicPopup, UserSrv,ProflieSrv,CameraService,$ionicActionSheet){
    UserSrv.hideSpinner();
    var vm = {};

    $scope.vm = vm;
    vm.address = {};

    $scope.$on('$ionicView.enter', function(viewInfo){
      getUser();
    });

    function getUser(){
      return UserSrv.get().then(function(user){
        vm.user = user;

        ProflieSrv.getUser(vm.user.userId).then(function(data){
            vm.user = data;
            vm.user.logged = true;
            UserSrv.set(vm.user);
            /*TODO: remove on production*/
            vm.user.Image = "data:image/jpeg;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAB5VSURBVHja7J13fBzlue+/72zT7qoXy90Y2cYGXMBgjE2xjY0NJEDqTUg5IZQDBFI4yUknlBAgCZ+Tc0PaTQ5pnCSEHHLuzeFDqC5g04wRuKgYY9xly5JVdqWt89w/ts3OzqxWsixLYR99Xu3szOzszvx+T3mftykRoSjvXdGKj6BIgKIUCVCUIgGKUiRAUYoEKEqRAEUpEqAo7xlxvldudNnZc8tFmA9MEqgEygGHQAdwVOAIwhHgwIuNW4Pvleei/lEzgSsWztVEWCZwDXAx0CCCAkjdcdZr5jHEBDaDbBDhBeCFTW9u6y4SYGyB/1ERvg9MS4Ms2aDbEUCS/w2PJQ68LPAw8OhLb24LFgkweoFfCNwLrBbJBttX4sLvc+NxO3FoCk3TQCmi8TixmE40GicS0+nrjxKKRI0EyFxH6BLk3wR+9Mpb23uKBBhd4N8J3JG4p8S+qgovdVU+Ksu9KE1lrIDJGqSegaAQhEgkTncgTG8wTHdviFAklv6AIAgcBb4B/OqVt7ZLkQAjLJcsnOsGVgJnA7MF5gFzU+CW+t00TK7G63WlAe4Pxwj0RQmFY8TiOjFdRwScDi1RnA6cTg2f14Xb6cDoOrp6+jl0tJee3nCKACl5VoTPvrp1+74iAUYG+FLgy8DngSrzL/e4nEydWEFNpQ9dhK7eEO2dfXR29xPXxToGsPD9JW4nZaUeynweyss8uF2JylJvX4RD7T10dPVlriEcEnj/a1u3v14kwIkFfyHwKNBgBFIpKHG78Ja4mDaxAo/bSaAvwtt7Own2R7GK+i0if6vgL324qsLLhJoy/H4PAD3BMLv3ddAfjqXOCQhcuXnr9rVFApwY8BcBzwN+ARQwrraUuiofpT53cg+EIzH2HOqmvbMvN4Czr/ZZgm8K/hAEv8/DhLoyqip86HGdXfs66ejuS53SjXDR5m3b3yoSYHiDu1nARqA2Fc03TK3G73WnzwmFY/SFouw+0EUoHBsYfAvTbwwOzZ81BH8AlJeWMH1SFW63k72HujjY3ps6bz/Iks3bduwrEmB4wK8F9SrIdIDaKh8NU6pRSnH0WB+HOwP0BiLokkd7B/T7hWm/+SkppWiYUkNlhZd39x/jcEcgcR3YAix+fduO6FggwGhvC/geyHSRhNY1TKkmGtNpeqed1j0ddPeGc8CXAsFnEODnfEIgrgute47S1t7LtImVVJV7U4fPBr5VtADHKcvPnrsQeBXQfF4XZ8wcR38oxo5dR4jHc012Pq0fkt/Po/3GJJMAp06qprrSx7adbfSHo5BIJy/Zsm3Ha0ULMARZdvZcJfDvAppAQvOjOs3vtBOLJUAbjNbbgQ/HDz7A7gOd9AbDnDq1BqVAwInwy7POOF0rEmBosgZYCjCuxo/b7WTHrnYiUT0HMGN10NYK2IBva/zymH7Lc0XYuacdp6aYOK48dWQ+8PEiAYYgItwgApqmmDy+gr2HunOiezMW08ZXWPv7AsG3J4v1eWarEteF3fs7mVBXjsflTB2666wzTncVCTAIufisuROA9wPU15QSicZpOxpIY2nl2wW4aMHkvCa/IPAzYWFe0597dkK6g2GOdAaZPL4idbBB4DNFAgxG++EzAk4B6qr97N7fZamFZjIsmFVPfY3fRuszAd/xBn3Z2p9Lif1tXZT7S/CWpBX/5iIBBicfTyRbPERiOr3BcBbgdhagrqacleeeagGuWEb7gwF/4NpEZn88Lhw62sOEurLkPjlrwelzzi0SoAC58Ky59QJnprT/cEfA1g8bTX2Fv4QSr5eVS06jzO8Z0OTbgZ/HKlm4CrH9bYc7ApT5PDgd6cd7Y5EABVb/SSb2/V4XHV399mbf8GbWtFrQXJSX+rn2qnOztN5Kg+3AL8jvSy74kr5OQnRdaD8WpLbSlzr+wfmnz3EUCTCw/18hgLfERVcqy2eO6C38wKxTxqGS9cDli2Ywb+bEgrTeaPaH6vetQgEB2jsD1FT6U7uqgfOLBBiYActJpn2P9YTsHb+JC5PrK1BKQykNlMbNH1tKqaGxyBJ8m2g/P/j2LsG8LxKNE4nGcbnSin9FkQB55IIFc30k2/pLfW56A2FbwI3BXXmpB7fLlcgEJcuEugru+fz7KPeX2Jr8oYEvli5BbDJGHV1BKsrSbQSriwTIb/5nCCgB4nGduMH8iwUFUv69utyX6PqV1P5UOXVKLfd+8X1UpgCQ/P5+sOCLBfhiiji7AiEqSktSx86cN2eOt0gAewrMAkHToC8ctQXcHNgppZh5yvi09qt00Zg2sZYHbr+a2dPrEQSXS7P390MAnzzgp4isZ/a5SPRdLBIAa+2bJZLo1xfsi9gCbhSXU2PqhGpcTkdWDJAoCUJMrK/kB1/5IJ+75mLcbhfja8uZc+p4aitLs6qIQwFfbDOJGenrj+B0OlLWYuFoI8BoGhrWAFDicdIbDBf2451Opk+pTQBusAjJLaOZ4PKL57F4QQN/fOI11r7Swqzp9SyaP51SnyfRlexgJ3sOdHC0Ozgo8M1+37w/0B/BX+KiKxoHOL1IAHupAHBoGtGYnr8Kl5RwJMqC2ZOzQU9tK5VDipqqMm795Aqu+/AFPP9yC0+s38qufe1Mn1zL2adPZfl5p1Fd4UfTFLFYnO5APz2BEIG+ED3BEL2BEL3BED3BMD2BEL19IXr7wqCLbQIp2B+hrqoU6Q0BMrVIAPsgsAwgFI1RaBcVv9fD9Ml1GQuQBFoZwDcbAwCv18MVy+dxxfJ5HDzSRWPTPhqb9/Hcy80c606M/Kqs8FHuL8FX4sbn9VDidiIC4WicaCxOXNfRdRmw9VCPC3FdTx2dUiSAvZQCxOK6LUPMMve0SSiHIxd0lb2RQwgDIybVVzOpvporliXisyMdPRw80sX+w120tXfTdrSbYH+EnkCI/kiMcCTR1a+y3Ed9TTklbidej4u1m3daJogkOzYsEiCPCSgFEN0u8ZO7c8GcqRlwlRXgue7AXhLnjKutZFxtJQvmZId4ma5zJq1PRqlv3v5LOjNdxC0zTyJUnTl7ttrW3CxFAuQC7AOI67ptw4x57/zZk9PAJ0C3ATyPBchvwA2kMsT8kkkqJL5XKcbXVtDZ3Wc52ijVhzFZ6yoB+ovVwNzHH5GkCxCLzJ+YQKup9DNpfLWp+qdMJbkfU1EWxfac3GtmvlOluTShttwiNZz4H4vrxurmqEoGjaYYoC9vDGDS0FOnjMuAkKXluZZAFWIBlKmRxzyUWJlTSCppFZIWwdAp0VwhNN1TkQA2MqBZNCaEpk6syWgludXALJdQUBygsr4oE1tIkgwq+QNSYEsWETq7+yzBF4FoIgeQOhYtEsAa3N4C3ERaxtVWJE13RtvzxgHkey95TlUZ/58iQ4oQBqvQ0RW0BB8gGosbj/UVCWAN7v5B1Rl9JWmNTwOfBXr+moAyoJwTdIopyatMZEgTIWXi4xxq77YE3xwPIKMnABxtLmDvYCyA1+tBaZoJeAtXYJsLsCaDkRBpYoiYyJBNhKZ3DhOORgsBP7S9pSVeJIA1uPsYBAN6AiFDG4DKAV0NVBVUyjq4yKKRMlT8TGQwEOHNln15wM9KH7QVE0H24O5iEAzo7ArkB97u1crRKxsXkHxVKahFkFTCyUCEzVt35wFfjLmjfUUC2IPbSGJKNkchLqAjRQClcoHPAr2QbKCy/iaVDPhUAvAcIgi0d/bQ2Ly3EPALcnPv2UTQ5m07ggLN1kmg3Dxd67ttKKWhKS2djUPTEsWiX4Cxo4hSGkozlHRyR2WuZUwkKc1w7cQ5WvIzT724zZjpy+69lNvJZG/RAgzAA+CMQixA066DHOsOUl1Vlp0PMDcDJ/erQbuAhPZL1vmSsQpJi/Dk+ka7gM+q0+j2ogXI7+E35LUAhjeiCy++3mzS1lTEr6E0R6Lk9BQauGSsROYaGfeSsSpN7xxkz4GjhYIPidlDigTIw4AnEPR8PsD4dsNrzYZcvhF4C9AxmP88JWFFTGTQNBMREseeXN+YMwopD/hBhNYiAfLIlu07Dgu8Wmgc8PrWXXR0BRK+2wL4HHCtfLpmo/3K4lppIihicZ2nX3wrK9jLAz4IbzS1jq4cwOizAAn5fwNVBFMlrgt/X7clDZAZxLQ2a7klC2SL42arYbw2SmPTlha6e4NZqOcBH4FROYfgaBwe/luBqH0ckN1d+H+e22xoFTSlhU1ga4ZirAVk7TeTwuK6Smk8sfZ1s/LnAx/gqSIBCpDG7TsOIvJ4Tr9wm/7hB9o6aHnnYLamGoA3gm1n9rMsg4EU2UTIkONYTx+bXmuyNPk24HcBrxQJULg8VIgLSJVNrzcltNMC+LQWD6omoNJkyCECiqfWb0l3XLHTehM5nm5ubYkVCVCoFdjR9KLA2oGCwJRsfG1HJkmDMgCfP8BTAwWKymAVDG7lb8++InnBz/2t/zlKFW1UTxT5pWRqeEBpfnsfXT3BjNZnJYWUBbgOkztw2ASQBr+ftAatuw+wc/dBlc/km8BvF+TJIgEGKW/uaHoT+GUhLkAXYUfzO31mU58TF2TlCRyZkqzeZZ1naREUv//TE7sLMPmGQ/LHltbWaJEAQ6sRfEPgHbtsoLE88eS6VyQWydL6jAlPgZ2b989qH1BJYmiOLCIkrAD0t+1k45aWkgJMfgp8HeFno/kZj2oCvLWj6RjChxD6jFpmVRpb9rl6d72C3t9j6LGrGYA3NvRYB37ZhHBkAkI9TnD3Ftp2t9ATjEwwa731NHMCwl9bdrY2FwlwPCRoamoUuEmyhl/mlu5gZLwejdC761UiXW2GdgBlSN86DP7eYfM+lVZOEEGP9NG7cxPR3g4adx0tROuNB+8b7c93TKwcurWp6fcg14PYBoWxmF4JILpOcM8b9B9sTnbbUllBXjo+MNT5c62BAxREjr5LT8uLxMOJfpz7jwRstT4Nfubg31p2tr5eJMCwkaD5YeAjIoQsc0RQBaTr2v1tO+na/jyRrrYs0LVkMfr99L4kGaI9h+nesY7A3reQeKb63tHTbwu8ZEeBIUnUYka9jKmlY7c2Nf/1zNmzrwD+L8nBpIZYzBHoj7aXel11qX3xcJDeXS/jKCnDW9+Ap2oKylNqqN5lIst4XzeRrv30H3mHeMi6h3pHT8he67N/y/2tO1t3FQlwAmRbc/PzZ8yefQvwu5ybcSqn1aCfeKiXwJ5GAnsacXhKcXjL0FyJATp6OEgs1IMeseitbbqWYeJHs683gr8NeGCsPM8xuni0PEJiVY5ZaayUipS4nRUDfTIeDhAPB4b0rZVlnnzAA/QAH27d2RoaK09yTC4fv725RTxuz3pjRcDrKek/0fcze2q15WTUBkpc17qztWUsPUuNMSoNM2e6jBXBmbMabLROHUfJlvPmjE+sOYylEXimdWfrX8bacxyzBHA6HA3G90vOP89vDfjxSPa1qsr9rLlsjUX7vyDIQ2PxOY5JAlx5+WXVcV1fbLQASy9aUaq0E7s4h/LVccONN+Dz+XKrf8KmIgFGQK5Yc+lH55x++ttH2tvTaM+fP5+pp5yC8tedWAKUjqe+vp6777kbpWXmA1BKMX3alOVFApxAuXz1qlmXr171PyUl3kcvu+J9VYcPtaXV/+oPfCABkL/eBjnTYA+7PoDG8ywJkLj+0qVL+NpXv5oZOyiCp8Tz2OWrV/358tWrThlTrnS0/8DLLl1ZD9wB3OByuVy33HYbb731VtoH19TUsGz5srSGFjYsbOAZQrKGiKfGCBoIduVVV+Lz+/jOd+4iFovRE+inosz3kWgkcuVll678CfC9J59+tqNoAYYK/KpLKi9bdcldiLyNyC0+r9f1hS/dzry581i7NtPB9sZ/vhGXK+ENtNLxoDkSxSL/b9UOYN8eYOw84kC5vChvddZvXLlyJQ8++ENKSkoI9fez4Kxz0BxOTzwWvx2Rty9bdcnXLlt1SVmRAIOQNStXTFmzcsWDIrJXRO4QkdIZM2Zy173f48y582hrayPQm0jkzJgxg8uvuDzz4ZJKlMufad7VsvP/xpy/udidk25W9o+z/L2LF5/HQw/9mGg0xv4D+/nJT37KgoVnIyKVInKfiOxZs3LFvWtWrhg/GgkwapaOvfSS5QuBz5NYNMoF4HA4eP+VV3HV1R/AkZwQet3atTz77LNs2LCBH/37j1i0aFF2pm/3c8ixndmmfyjTxJmGiGsTzkWbYD/X8/79+7nvvgeoranhG9/8Bm82vsFvf/1rOjvTXiBEIn390NPPrd1aJABw6YplFcAngOtILLqcljPOPJNPfuqfmDhpUuKHaon2+Rc2bOCvj/+Vw4cP8+hjj+bC2NlKfO86E9BDmScwe5oY56yrwTfO9l5SU8Y89fTT7N+/n9WrV1NRXs6f//RHnnv2GfPpm4D/AB59+vl1wfccAVYtv3gm8BXgGsCfc3z1Gq755KfQVGZUr9IUbW2HiYTDfPee77J8xXI+/U+fzr14tI/Y9j9kA22aO8gqyJecdWEyBFAOD465n8ybWEoRQETQRejt7cHvS9zayy9t4qcP/djqYz1Jq/DDZ9au3/MPHwOsXHbRnJXLLvpPEWkSkRtExC+SmGfHWI62t5MzdZNA26FDTDvlFELhELNnz7H+EpcvEawZxvJpWcUw6tdQEr4/c16q8yhKQ5VPGjiraJodtNSfaa1etPh8Jk2ejMW9lovIrSKyc+Wyi/5j5bKLGv5hCXDJxRfeJCJbROQaEXFYAZ8qr29+jaf//veca3hKPCilqKysora2xv6myidng5hvVJDVYFHNRJ6yyUO+bxHhD4/8jn1795Lnnl0i8lkRabzk4gs/8Q+VB1h+4VJf0t99bDCf+/1vf0MkEuGqqz+Q3jejYQYAtXW1uFwuxDihY1bCZiLS2YL1RJEqNwwwrxwthpk/lUDZxAFBNr6mpLuri1/8/Gc0vlHwtAClwCPLL1x6EXDb2hc2RsZ0DLD8wiVuEr131gz1GgvPOZebP3cbPp8vHQg+9dRTzJwxg5mzZlpP/6bH0FseSwJqM4N4HjtunBlcuUpRM64cmAAG/y+is/GFDfzm4YcJBHqHeuuPAtesfWGTPiYJsOyC8zXgkWS17rikfvx4bv+XrzDtlOkoTREKhTmwfz+zZs3K5OTNsvc5pO+oPQHyWIAsAlTNgPr8S/1IcsUQXYSurmP86hc/59VXXh6Ox/iTdS++dOuYJMDFS8//KnD/cF3P7Xbz6c9cy6Vr1qCUhug6DocjOXLLggQdTXB0e55cQF6VzrxOWgKlEwfU/ng8znPPPsMfHvk9vb29w/kor1u/8aWHxxQBLjz/vNnAGyTmxh9WOXPuXG665VYmTpyYVUXMkVAn7FuXGwOk5xS0MgDmGcI1OPV9oDnzan9zUxO/+j+/YNeut08ERl3AGS+89MrBMUGACxYvUsBGTuBauW63m49d84lEhjCfFXj3SVQ8bJhPeBAxgAjirYOJS23Pbm9v53e//jUb1q870bHa4y++/OqHxgYBzjv3cuCJkajCnNrQwK1f+CKnntpgSQKtYyv0vEvuRNK2NsBgAQSpm4+U5i70dbitjcf/8hjPPfMM0eiIjPsUYOGLr7z2xqgnwNJF52wEloxUPdbhcPDpz1zL1R/8UA4JVKgT1fayTfCn7LI4iQ2lIVNWIibz337kCDffcD2RSIQRlj9vfHXz/xrViaAlixYuFmSJMHJ/sXiM7du3pXEzElpKqsHpTSR2HMlikQVMl+Q5aBr46nPAT+Qg6rjx5luQkf/70JJFC6eN6kSQrusfH2m1qKyq4rob/jmTFEouEJ22BP7xqMCB3LUEcgyAcU5QQbeI/FMR/6pLV3PwwAEe+/OfRvJWHcBHgR+MSgtw3sIFmoh8OF+Kd7iL2+3mW3fcSd24uuT6fJJjCcQ/MdlBxKj91hNEpCeJcJZASZ0l+KntT33mWpZecCEjeb8i8pFR6wJ0XT9L1/WJuq4zUuXGm26hoWFG8uHouSTQBd1VnsjmDWKKGPwTkeQwcRFJJHoM4Ce+R+fa669P3ftIlXPOPWvehFHpAkSX80fSHpaXl7N8xYpMJk4D0JFkV02Vyv4J6L4JaIF37dcUEuOagBD3TczS+GzgU8vZC5UVVZSXV3Css3Okblslq9ePjz4CiCweSQL09PSws7WV05LNwqInV/FQkjBsSqWB1b3jcPQfMK0FZb1iiLgqEc2TpfGpCCFlaZBEQ8+DP3iAzo4R7/e5ZDgJoA0fAfTTRHRGquh6nK//65ezettkGmWS5yS1No4T8VQlTb4DHNnRP45kR1JNI+4bn/a5qc+nGnhS4L/Z+Aafu/lGNr/2KiN5z8kyd3S6ABn5pdFDoRAPfv9+mpt2cONNt6SbiNONgCrTGhj11OPVg5a1AJWsBegOHzFHmUHzxUQs4fe//Q1/+sMjnMSudNNHXSJo/hmzNSDKSexlfNrsOXzr29+hblx2vz3j4tKl4d1oegi7PoH9nqnEHKWWUX80GuWH37+f9etO+pzPEcD75vZmfdQQAGDe6aftAaaezCdTUVHB17/5bc4627r51i0BvNFDlgSIayUEXVOy3UlSgsEAd33nDt5sfINRIO1v7WgZNwpjAPnbCNeJc0pXVxff/PpXeWnTxvQ+o0RVKaKVJAZ6aM50QXMQctRmXSslx4518qXP30bjG1s42feXLH8blW0BZ86eOQFoBMadbBVxOp18+867OW/x+Ynp3gzuwEMAn34sywjEKKFX5SZ++vr6+JcvfYFdb+9klMheYOG25p1HTygBnGpw4+pjyWucMavhHOBZoOJkPymXy8Udd9/DovMWY+wVpIAKrR1HehpiRbfUEhdHVptgNBrlm1/7Vxq3jJqZ3rYCV2xv3bVvsBjF8ij5sBLAqZSa2XBKg1LqT8DCk04Ct5v7Hvghc+fPywoIPVqUckdPwr/rfvriXmOjACLCfffczbq1z48G4EPAA9Fo7P6d7+4NDQWjE0UAZbddU13pqKqs+KhS6mvkWQZuRDKGFRX8+Cc/Z/yECVlNxVWePpQSjoX9OZ2Cf/7Th3j8L4+dbODfFZFfxGLxX7+7d/9R7BclkZEggMXqzFlzsCiLc3A6HNqUyRPe73A4vgycc7Ke5JSpU/nfD/0Mf2lmsIZTS9x3TM++1/Xr1nLv3XeerJ8aFeE5XY//6sChI3+PRCJxC6DFYlsszhk2AqTA1QZ4xbBtfs+E+rrTPB73pZqmrVJKnUueJWJPhJx9zjl87/4fJLqQ2cihgwe56YbP0tfXN1I/S0Rkp4hsiMXi67u6ezb2BoIBA4i6CWjddCzfqwwHAVIAOyxKar8yAa9Z7MsqpX5fVXlZ6cVOl/MCTakzlFLTRiKJ9P6rruK2L96eXP3DpHrRKF/43C2c4FneArrI26LrLbFYvDEQ7NvU0xs4bKHNYgLdDGxqO57cjluU1DlDIoBRg7VkythlKE4LMijD+XkJYHYjbrfLV+r3z/K4XbMcDsdpmqbN0jQ1BfAMNwI333obV33ggzn9B3/20I/578f/a7i+JqiLHBZdfzce13dFY7G3Q6Hw24FAsE1ytVoGQQDjthHsWDITmyoxw/nE8tT1CyGAIwm6B3AnS4oETpMV0AyEMFoQ8pDA6hVA85Z4qj0e9zin0znO4XDUOzStXtPUeKVUjVKqTClVBngZxHxwmqZx53fv5dxzz0vve2njRu6+89uFfLxfRIIi9AjSK7p06Lp+JK7r7fF4/Eg0GjsSCoePRCLRgIW/Jo+vtloaSTdtx00kSAGfAj+SLOHk+/jxEkCZCOA2kMBMgJS2OwxEwMISkGfb6j15CJIEVDldLpff6XCUOp2OMoemlSlNeZVSTgVOlHIp0JKvLsBRWVnlufb6G05TSumIxH77m4e3th9pDwoSBeIiEhNdQnFd743H9WAsFuuNRGMBXddjA0TfMsAxu1XmrLbNBEiRwGgBzAQIJ1+NBJDhsABOAwnM4JtdgMMmECyk5mA3w6NdlZMCtX+wM0bKEM6RAY5JHs3HxvQb98dtXICZBCkXUJAFcOa5OWX6shQTY8nPaSaNLzgIzAN2PgLkG9mhhhH8QkgwkPYPRIChxgBiigfiBkxiVgHgQGR2FnCT5iAklgf0QifgHUj78xEgnxUYDiIUAvxA2p+PAIOxAgORQiziAp2Bl1osiADGH2S0BgMlgwby9YW+Mkh3oIbJDcggjhdi9gsJAAcixEAWw45kHC8B8jHfzhQXAopWADhqmDX8RBCgkOckBVxDL5BshVxrUM27g+kSNtAXKZPFyAdC/AT48eOdGvxEBojHe8z4TIe1L5rzBD0YGcLNjxSAJ1tkhD+XX2tGy0SRRTk5ohUfQZEARSkSoChFAhSlSICiFAlQlPeY/P8BAFOlWb58HhZeAAAAAElFTkSuQmCC";
        });
      });
    };


    
    
    $scope.updateUser = function (user) {
        
        UserSrv.showSpinner();
        var constructCustomer = {
            "userId" : user.userId,
            "fullName":user.fullName,
            "emailId": user.emailId,
            "phoneNumber": user.LoginId,
            "address1": user.address1,
            "address2": user.address2 || '',
            "city": user.city,
            "state": user.state||'',
            "country": user.country,
            "zipCode": user.zipCode,
            "latitude": user.latitude,
            "longitude": user.longitude
        }
        
        var data = new FormData();
        if(user.Image){
            var b64 = user.Image.split(',');
            var file = new File([window.atob(b64[1])], 'file.jpeg', {type: 'image/jpeg'});
            data.append("image1", file);
        }
       
        var jsonString = JSON.stringify(constructCustomer);
        data.append("json", jsonString);

        ProflieSrv.updateUser(data).then(function(){
            UserSrv.hideSpinner();
            getUser()
            var alertPopup = $ionicPopup.alert({
                 title: 'Success',
                 template: 'SuccessFully saved your data'
               });
            alertPopup.then(function() {
                $state.go('app.category');
            });
        },function(error){
            printError(error.Message);
        });
        
    }
  
    $scope.uploadImage = function () {
        //open poup using ionicActionSheet
        var hideSheet = $ionicActionSheet.show({
            buttons: [
            { text: 'Take photo' },
            { text: 'Photo from library' }
            ],
            titleText: 'Add images',
            cancelText: 'Cancel',
            buttonClicked: function (index) {
                CameraService.getPicture(index).then(function (imageUrl) {
                    vm.user.Image = imageUrl;
                    $scope.updateUser(vm.user)
                    hideSheet();
                });
            }
        });
    }

    
    $scope.$watch('vm.address',function(newValue, oldValve) {

        if(newValue != oldValve){
            if(newValue && newValue.geometry){

                vm.user.latitude  =  newValue.geometry.location.lat(); 
                vm.user.longitude = newValue.geometry.location.lng() ;

                _.each(newValue.address_components,function(data){
                    if(data.types.indexOf("postal_code") >-1){
                        vm.user.zipCode = Number(data.short_name);
                    }

                    if(data.types.indexOf("country") >-1){
                        vm.user.country = data.short_name;
                    }

                    if(data.types.indexOf("administrative_area_level_1") >-1){
                        vm.user.state = data.short_name;
                    }

                    if(data.types.indexOf("administrative_area_level_2") >-1){
                        vm.user.city = data.short_name;
                    }

                    if(data.types.indexOf("locality") >-1){
                        vm.user.address2 = data.short_name;
                    }

                    if(data.types.indexOf("sublocality_level_1") >-1){
                        vm.user.address1 = data.short_name;
                    }


                })

            }
        }
        
    })

    function printError(mes){
      UserSrv.hideSpinner();
      vm.error = mes || "Something went wrong, Please check all credentials and try again";
      var alertPopup = $ionicPopup.alert({
       title: 'Message',
       template: vm.error
      });
    }

  }
})();
