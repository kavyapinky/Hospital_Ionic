(function(){
  'use strict';
  angular.module('app')
  .controller('insuranceCtrl', insuranceCtrl);

  function insuranceCtrl($scope,$state){
    var vm = {};
    $scope.vm = vm;
    $scope.schemes =[
      {name: "apply insurance", value: "Insu"},
      {name:"skip insurance" , value: "skip"}
    ];

    $scope.data = {
       clientSide: 'Insu'
  };

    $scope.goSomewhere = function () {
    var path;
    switch($scope.data.clientSide) {
      case 'Insu': path = 'insurancePlan'; break;
      case 'skip': path = 'patientProfile'; break;
    }
    $state.go(path);

  };

  }
})();
