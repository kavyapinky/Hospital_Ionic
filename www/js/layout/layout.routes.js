(function()
{
  'use strict';
  angular.module('app')
    .config(configure);

  function configure($stateProvider){
    $stateProvider

  .state('main', {
    url: '/main',
    templateUrl: 'js/layout/scheduleApp.html',
    controller: 'scheduleAppCtrl',
    cache : false
  })

  .state('patientSchedule', {
    url: '/patientSchedule',
    templateUrl: 'js/layout/patientSchedule.html',
    controller: 'patientScheduleCtrl',
    cache : false
  })

  .state('hospital', {
      url: '/hospital',
      templateUrl: 'js/layout/hospital.html',
      controller: 'hospitalCtrl',
      cache : false
  })
  .state('physician', {
      url: '/physician',
      templateUrl: 'js/layout/physician.html',
      controller: 'physicianCtrl',
      cache : false
  })
  .state('calander', {
      url: '/calander',
      templateUrl: 'js/layout/calander.html',
      controller: 'calanderCtrl',
      cache : false
  })
  .state('timeSlotPop', {
      url: '/timeSlotPop',
      templateUrl: 'js/layout/timeSlotPop.html',
      controller: 'calanderCtrl',
      cache : false
  })
  .state('insurance', {
      url: '/insurance',
      templateUrl: 'js/layout/insurance.html',
      controller: 'insuranceCtrl',
      cache : false
  })
  .state('insurancePlan', {
      url: '/insurancePlan',
      templateUrl: 'js/layout/insurancePlan.html',
      controller: 'insurancePlanCtrl',
      cache : false
  })
  .state('patientProfile', {
      url: '/patientProfile',
      templateUrl: 'js/layout/patientProfile.html',
      controller: 'patientProfileCtrl',
      cache : false
  })
  .state('editProfile', {
      url: '/editProfile',
      templateUrl: 'js/layout/editProfile.html',
      controller: 'editProfileCtrl',
      cache : false
  })
  .state('summary', {
      url: '/summary',
      templateUrl: 'js/layout/summary.html',
      controller: 'summaryCtrl',
      cache : false
  });
  }
})();
