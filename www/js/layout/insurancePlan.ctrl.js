(function(){
  'use strict';
  angular.module('app')
  .controller('insurancePlanCtrl', insurancePlanCtrl);

  function insurancePlanCtrl($scope, $state){
    var vm = {};



    $scope.vm = vm;

     var selectedPatient = localStorage.getItem('selectedPatient');
     selectedPatient = JSON.parse(selectedPatient || {});

    $scope.plans =[
        {name: "Senior Citizen Insurance"},
        {name:"Maternity Insurance"},
        {name:"Family Insurance"}
     ];

     localStorage.plans = JSON.stringify($scope.plans);
     var app = JSON.parse(localStorage.plans);

     $scope.chooseInsurence = function(plan){
        $scope.selectedInsurence = plan;
    }

    $scope.proccedTonext = function(){

        if(!$scope.selectedInsurence){
          /*TODO implement toast*/
    }

      selectedPatient.plan = $scope.selectedInsurence;
      localStorage.setItem('selectedPatient', JSON.stringify(selectedPatient));
      $scope.selectesInsurance = JSON.parse(localStorage.getItem('selectedPatient'))

      $state.go('patientProfile');
    }

    $scope.init = function(){
        $scope.selectesInsurance = JSON.parse(localStorage.getItem('selectedPatient'))
    }
    }
})();
