(function(){
  'use strict';
  angular.module('app')
    .controller('StartCtrl', StartCtrl);

  function StartCtrl($scope,$state,$translate,UserSrv,$timeout){
    var vm = {};
    $scope.vm = vm;


    
    $scope.$on('$ionicView.enter', function(viewInfo){
      //UserSrv.showSpinner();
      $timeout(function() {
        $state.go('login');
        //UserSrv.hideSpinner();
      }, 3000);
    });

    $scope.goToLoginScreen = function () {
        $state.go('login');
    };

    $scope.changeLanguage = function () {

        UserSrv.get().then(function(user){
            if (user.lang == 'es') {
                user.lang = 'en';
            }
            else {
                user.lang = 'es';
            }
            UserSrv.set(user);
            $translate.use(user.lang);
        });

        
         
    };
    
  }
})();
