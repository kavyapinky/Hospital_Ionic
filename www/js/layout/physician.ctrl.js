(function(){
  'use strict';
  angular.module('app')
  .controller('physicianCtrl', physicianCtrl);

  function physicianCtrl($scope,$state){
    var vm = {};
    $scope.vm = vm;
    var selectedPatient = localStorage.getItem('selectedPatient');
    selectedPatient = JSON.parse(selectedPatient || {});

        $scope.physicians = [
            {name: "Dr. Arun Kumar"},
            {name: "Dr. Arpan Dev Bhattacharya"},
            {name: "Dr. Anuradha Vinod"},
            {name: "Dr. Anoop Amarnath"}
        ];

        localStorage.physicians = JSON.stringify($scope.physicians);
        var app = JSON.parse(localStorage.physicians);

        $scope.choosePhysician = function(physician){
            $scope.selectedPhysician = physician;
        }

        $scope.proccedTonext = function(){
            if(!$scope.selectedPhysician){
            /*TODO implement toast*/
        }

            selectedPatient.physician = $scope.selectedPhysician;

            localStorage.setItem('selectedPatient', JSON.stringify(selectedPatient));
            $scope.selectedphysician = JSON.parse(localStorage.getItem('selectedPatient'))

            if(selectedPatient.hospital)
            {
                $state.go('calander');
            }
            else
            {
             $state.go('hospital');
            }
        }

        $scope.init = function(){
          $scope.selectedphysician = JSON.parse(localStorage.getItem('selectedPatient'))
        }

    }
})();
