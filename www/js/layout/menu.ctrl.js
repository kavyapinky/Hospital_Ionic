(function(){
  'use strict';
  angular.module('app')
    .controller('MenuCtrl', MenuCtrl);

  function MenuCtrl($scope, $state, AuthSrv,UserSrv, $cordovaAppRate,$ionicPopup){
    var vm = {};
    $scope.vm = vm;

    vm.logout = logout;
    vm.goToLogoutScreen = goToLogoutScreen;
    vm.goToLandingScreen = goToLandingScreen;
    vm.goToNewRequestScreen = goToNewRequestScreen;
    vm.goToMyRequestScreen = goToMyRequestScreen;
    vm.rateApp = rateApp;
    vm.isSkip = AuthSrv.isSkip;
    vm.navigateWithLogin = navigateWithLogin;

    
    $scope.$on('$ionicView.enter', function(viewInfo){
      getUser();
      vm.isGuestUser = !(AuthSrv.isLogged());
    });

    function navigateWithLogin(link){
      if(AuthSrv.isSkip)
      {
        var alertPopup = $ionicPopup.alert({
             title: 'Message',
             template: 'Please login to avail this services'
           });
        alertPopup.then(function() {
            $state.go('login');
        });
      } 
      else{
        $state.go(link);
      } 
    }


    function rateApp()
    {
      $cordovaAppRate.navigateToAppStore().then(function (result) {
          // success
      });
    }
    function logout(){
      AuthSrv.logout().then(function(){
        AuthSrv.isSkip = true;
        $state.go('login');
      });
    };


    function getUser(){
      return UserSrv.get().then(function(user){
        vm.user = user;
      });
    };

    function goToLogoutScreen() {
        
        AuthSrv.logout().then(function(){
          $state.go('login');
        });
    };

    function goToLandingScreen() {
        $state.go('app.category');
    };

    function goToNewRequestScreen() {
        $state.go('app.new-request');
    };

    function goToMyRequestScreen() {
        $state.go('app.my-requests');
    };

  }
})();
