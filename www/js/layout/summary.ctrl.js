(function(){
  'use strict';
  angular.module('app')
  .controller('summaryCtrl', summaryCtrl);

  function summaryCtrl($scope, $state)
  {
    var vm = {};
    $scope.vm = vm;
      $scope.selectedPatient = localStorage.getItem('selectedPatient');
      $scope.selectedPatient = JSON.parse($scope.selectedPatient || {});

    $scope.editSelectPatient = function(){
      localStorage.setItem('selectedPatient', JSON.stringify($scope.selectedPatient));
      $state.go('main');
    }
    $scope.editSelectphysician = function(){
      localStorage.setItem('selectedPatient', JSON.stringify($scope.selectedPatient));
      $state.go('physician');
    }
    $scope.editSelectHospital = function(){

      localStorage.setItem('selectedPatient', JSON.stringify($scope.selectedPatient));
      $state.go('hospital');
    }
    $scope.editSelectInsurance = function(){
      localStorage.setItem('selectedPatient', JSON.stringify($scope.selectedPatient));
      $state.go('insurancePlan');
    }
    $scope.editSelectBokingDate = function(){
      localStorage.setItem('selectedPatient', JSON.stringify($scope.selectedPatient));
      $state.go('calander');
    }
    $scope.editSelectTimeSlot = function(){
      localStorage.setItem('selectedPatient', JSON.stringify($scope.selectedPatient));
      $state.go('timeSlotPop');
    }
  }
})();
