(function(){
  'use strict';
  angular.module('app')
  .controller('calanderCtrl',calanderCtrl);

  function calanderCtrl($scope,$ionicPopover,$state){
    var vm = {};
    $scope.vm = vm;
    var selectedPatient = localStorage.getItem('selectedPatient');
    selectedPatient = JSON.parse(selectedPatient || {});

    $scope.slots =[
     {time: "10AM-1PM"},
     {time:"3PM-5PM"},
     {time:"7PM-8PM"}
   ];

   localStorage.slots = JSON.stringify($scope.slots);
   var app = JSON.parse(localStorage.slots);

   $scope.chooseTimeslots = function(slot){
    $scope.selectedTimeslots = slot;
  }

  $scope.proccedTonextPage = function(){

    if(!$scope.selectedTimeslots){
      /*TODO implement toast*/
    }

    selectedPatient.slot = $scope.selectedTimeslots;

    localStorage.setItem('selectedPatient', JSON.stringify(selectedPatient));
    $state.go('calander');

    $scope.closePopover();
  }
    $scope.currentDate = new Date();

    $scope.datePickerCallback = function (val) {
      if(typeof(val)==='undefined'){
        console.log('Date not selected');

      }else{
        localStorage.setItem('val', val);
        console.log('Selected date is : ', val);
        $scope.bookingDate = val;
        $scope.openPopover(event);
      }
    };

    $ionicPopover.fromTemplateUrl('js/layout/timeSlotPop.html', {
      scope: $scope
    }).then(function(popover) {
      $scope.popover = popover;
    });


    $scope.openPopover = function($event) {
      $scope.popover.show($event);
            //
    };

    $scope.closePopover = function() {
      $scope.popover.hide();
    };

    $scope.proccedTonext = function(){

      selectedPatient.bookingDate = $scope.bookingDate;
      localStorage.setItem('selectedPatient', JSON.stringify(selectedPatient));
      $state.go('insurance');
    }
  }
})();
