(function(){
  'use strict';
  angular.module('app')
  .controller('patientProfileCtrl', patientProfileCtrl);

    function patientProfileCtrl($scope, $state){
    $scope.selectedPatient = localStorage.getItem('selectedPatient');
    $scope.selectedPatient = JSON.parse($scope.selectedPatient || {});

    $scope.proccedTonext = function(){
      localStorage.setItem('selectedPatient', JSON.stringify($scope.selectedPatient));
      $state.go('summary');
    }
  }
})();
