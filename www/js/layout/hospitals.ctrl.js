(function(){
  'use strict';
  angular.module('app')
  .controller('hospitalCtrl', hospitalCtrl);

  function hospitalCtrl($scope,$state){
    var vm = this;
    var selectedPatient = localStorage.getItem('selectedPatient');
    selectedPatient = JSON.parse(selectedPatient || {});

    $scope.hospitals = { index: 0 };
      $scope.hospitals =
      [
          {name: 'Manipal Hospital'},
          {name: 'Appolo Hospital'},
          {name: 'Sparsh Hospital'},
          {name: 'Sree Lakshmi Hospital'},
      ];

      $scope.chooseHodpital = function(hospital){
          $scope.selectedHospital = hospital;
      }

      $scope.proccedTonext = function()
      {
          if(!$scope.selectedHospital){
          /* TODOimplement toast*/
          }
          selectedPatient.hospital = $scope.selectedHospital;
          localStorage.setItem('selectedPatient', JSON.stringify(selectedPatient));
          $scope.selecthospital = JSON.parse(localStorage.getItem('selectedPatient')).hospital;
          if(selectedPatient.physician)
          {
              $state.go('calander');
          }
          else
          {
              $state.go('physician');
          }
      }
      $scope.init = function(){
          $scope.selecthospital = JSON.parse(localStorage.getItem('selectedPatient')).hospital;
      }
      $scope.init();
    }
})();
