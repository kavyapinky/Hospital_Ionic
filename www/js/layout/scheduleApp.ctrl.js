(function(){
	'use strict';
	angular.module('app')
	.controller('scheduleAppCtrl', scheduleAppCtrl);

	function scheduleAppCtrl($scope, $state){
		var vm = {};
		$scope.vm = vm;
		$scope.appoinmentFor = [
     {
        name : 'Johny Addison',
        relation : 'My self ',
        age : '40'
		},
    {
        name : 'Adele Addison',
        relation : 'Wife',
        age : '30'
    },
    {
        name : 'Alexender Addison',
        relation : 'Son ',
        age : '20'
    },
    {
        name : 'Alice Addison',
        relation : 'Daughter ',
        age : '15'
    }
    ]

    $scope.init = function(){
        $scope.selectedpatient = JSON.parse(localStorage.getItem('selectedPatient'));
    }

     $scope.selectPateint = function(patient){
        localStorage.setItem('selectedPatient', JSON.stringify(patient));
        $scope.selectedpatient = JSON.parse(localStorage.getItem('selectedPatient'));
        $state.go('patientSchedule');
        }
    }
})();
