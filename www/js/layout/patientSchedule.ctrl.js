(function(){
  'use strict';
  angular.module('app')
  .controller('patientScheduleCtrl', patientScheduleCtrl);

  function patientScheduleCtrl($scope, $state, $location){
    var vm = {};
    $scope.vm = vm;
    $scope.choosePateintSchedule = [
        {name : 'Choose Hospital', value: "hosp" },
        {name : 'Choose Physician', value: "physic"}
    ];

    $scope.data = {
     clientSide: 'hosp'
    };

    $scope.goSomewhere = function () {
    var path;
    switch($scope.data.clientSide) {
      case 'hosp': path = 'hospital'; break;
      case 'physic': path = 'physician'; break;
    }
    $state.go(path);
  };
}
})();
