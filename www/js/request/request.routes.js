(function(){
  'use strict';
  angular.module('app')
    .config(configure);

  function configure($stateProvider){
    $stateProvider
    .state('app.request', {
      url: '/request',
      cache: false,
      views: {
        'menuContent': {
          templateUrl: 'js/request/request.html',
          controller: 'PlaceRequestCtrl'
        }
      }
    })
    .state('app.selectVendors', {
      url: '/selectVendors',
      cache: false,
      views: {
        'menuContent': {
          templateUrl: 'js/request/selectVendors.html',
          controller: 'SelectVendorCtrl'
        }
      }
    })
    .state('app.summary', {
      url: '/summary',
      cache: false,
      views: {
        'menuContent': {
          templateUrl: 'js/request/summary.html',
          controller: 'SummaryCtrl'
        }
      }
    })
    .state('app.requestComplete', {
      url: '/requestComplete',
      cache: false,
      views: {
        'menuContent': {
          templateUrl: 'js/request/request-complete.html',
          controller: 'ServiceRequestCompleteCtrl'
        }
      }
    })
    .state('app.myRequestTab', {
      url: '/tabs',
      cache: false,
      views: {
        'menuContent': {
          templateUrl: 'js/request/myRequestTab.html',
          controller: 'myRequestTab'
        }
      }
    });

  }
})();
