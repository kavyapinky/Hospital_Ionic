(function(){
  'use strict';
  angular.module('app')
    .factory('RequestSrv', RequestSrv);

  RequestSrv.$inject = ['$http','Config','PushPlugin','$q','SubCategoryService'];

  function RequestSrv($http, Config, PushPlugin,$q,SubCategoryService){
    var service = {
      postRequest: postRequest,
      openRequests: openRequests,
      closedRequests: closedRequests,
      inProgressRequests: inProgressRequests,
      get:get,
      postRating:postRating,
      getUserRating:getUserRating,
      getOrganization:getOrganization,
      getAllUserRating:getAllUserRating,
      getVendors:getVendors
    };
    return service;

    function postRequest(requestObj,msg){

      return $http.post(Config.backendUrl +  Config.apiKey.request, (requestObj)).success(function (response) {

        $http.get(Config.backendUrl  + Config.apiKey.providerUser).success(function (users) {
          
          var pushids = []
          angular.forEach(users,function(data){
            pushids.push(data.RegisterId);
            $http.post(Config.backendUrl+  Config.apiKey.sendNotification + '?alias='+data.UserName+'&message='+msg, JSON.stringify({}));
          });
          PushPlugin.sendPush(pushids, {title:'New Request',message : msg});

        });

        return response;

      });
    }

    function postRating(requestObj,msg){
      return $http.post(Config.backendUrl +  Config.apiKey.rating, (requestObj)).success(function (response) {

        $http.get(Config.backendUrl + Config.apiKey.sendNotificationByUserId + '/'+requestObj.ForUserId+'/notification/'+msg);

      });
    }

    function getUserRating(userId,requestId){
      return $http.get(Config.backendUrl + Config.apiKey.rating +'/'+userId+'/request/'+requestId);
    }

    function getAllUserRating(userId){
      return $http.get(Config.backendUrl+ Config.apiKey.rating + '/'+userId+'/foruser');
    }

    function getRequest(requestObj) {
      var deferred  = $q.defer();
      var request = [];

      $http.post(Config.backendUrl+ 'RequestTransaction', JSON.stringify(requestObj)).then(function(res){

        _.each(res.data,function(res){
          res.BiddingDetails = JSON.parse(res.BiddingDetails);
          res.ChatDetails = JSON.parse(res.ChatDetails); 

          SubCategoryService.get(res.CategoryId).then(function(subCategory){
            res.subCategory =subCategory;
          });
        })
        deferred.resolve(res.data);
      });
      return deferred.promise;
    }



    function getVendors(){
      return $http.get(Config.backendUrl + 'provider/organization');
    }

    function openRequests(req) {
      req.Status = 'open';
      return getRequest(req);
    }
    function closedRequests(req) {
      req.Status = 'close';
      return getRequest(req);
    }
    function inProgressRequests(req) {
      req.Status = 'inProgress';
      return getRequest(req);
    }

    function get(requestId) {
      return $http.get(Config.backendUrl +  Config.apiKey.request +'/'+requestId);
    }

    function getOrganization(userid){

      return $http.get(Config.backendUrl+ Config.apiKey.organizationByUserId +'/'+ userid).success(function (response) {
        return response;
      });
    }


  }


})();
