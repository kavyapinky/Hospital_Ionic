(function(){
  'use strict';
  angular.module('app')
  .controller('myRequestTab', myRequestTab);

  function myRequestTab($scope, UserSrv,RequestSrv,$state,$ionicPopup,$http,Config,$ionicTabsDelegate){
    var vm = {};
    $scope.vm = vm;
    $scope.vm.header= 'Open Request';
    UserSrv.hideSpinner();

    $scope.vm.rating= {
      rate :0,
      max :5,
      comments:''
    }

    $scope.inProgressRequests = [];
    $scope.closedRequests = [];
    $scope.openRequests = [];
    var req = {};

    $scope.$on('$ionicView.enter', function(viewInfo){
      getUser();
    });

    function getUser(){
      return UserSrv.get().then(function(user){
        vm.user = user;

        req ={
          "UserId": vm.user.Id,
          "CateroryIds": [],
          "Status" : 'open'
        }

        
        $scope.closeRequestData();
        $scope.inProgressRequestData();
        $scope.openRequestData();
      });
    };

    
    

    $scope.openRequestData = function () {
      if(!req.UserId)
        return;

      UserSrv.showSpinner();
      $scope.vm.header= 'Open Request';
      RequestSrv.openRequests(req).then(function(res){
        UserSrv.hideSpinner();
        $scope.openRequests = res; 
      },function(error){
        UserSrv.hideSpinner();
        $scope.vm.error = error.data && error.data.ExceptionMessage ? error.data.ExceptionMessage : 'Something went wrong, please try again ';
        var alertPopup = $ionicPopup.alert({
          title: 'Message',
          template: $scope.vm.error
        });
      });
    };


    $scope.closeRequestData = function () {
     if(!req.UserId)
      return;
    UserSrv.showSpinner();
    $scope.vm.header= 'Close Request';
    RequestSrv.closedRequests(req).then(function(res){
     UserSrv.hideSpinner();
     $scope.closedRequests = res; 
   },function(error){
     UserSrv.hideSpinner();
     $scope.vm.error = error.data && error.data.ExceptionMessage ? error.data.ExceptionMessage : 'Something went wrong, please try again ';
     var alertPopup = $ionicPopup.alert({
      title: 'Message',
      template: $scope.vm.error
    });
   });
  };

  $scope.inProgressRequestData = function () {
    if(!req.UserId)
      return;
    $scope.vm.header= 'Inprogress Request';
    UserSrv.showSpinner();
    RequestSrv.inProgressRequests(req).then(function(res){
     UserSrv.hideSpinner();
     $scope.inProgressRequests = res; 
   },function(error){
     UserSrv.hideSpinner();
     $scope.vm.error = error.data && error.data.ExceptionMessage ? error.data.ExceptionMessage : 'Something went wrong, please try again ';
     var alertPopup = $ionicPopup.alert({
      title: 'Message',
      template: $scope.vm.error
    });
   });
  };

  $scope.goToBiddingDetails = function (request) {
    $state.go('app.bidding', { "requestId": request.Id });
  };

  $scope.onTabSelected =function(name,type){
    $scope.vm.header= name;
  }

  $scope.selectBidder = function (request) {
    UserSrv.showSpinner();
    var req = angular.copy(request);
    req.Status = 'close';
      // req.BiddingDetails = JSON.stringify(req.BiddingDetails);
      // req.ChatDetails = JSON.stringify(req.ChatDetails);

      $http.post(Config.backendUrl + Config.apiKey.request +'/'+ req.Id +'/status?spId='+req.SPId+'&status=close&userId='+vm.user.Id, {}).success(function (response) {
        UserSrv.hideSpinner();
        var alertPopup = $ionicPopup.alert({
          title: 'Message',
          template: 'Task completed successfully'
        });
        alertPopup.then(function(){
         $ionicTabsDelegate.select(2);
       });


      },function(error){
        UserSrv.hideSpinner();
        $scope.vm.error = error.data && error.data.ExceptionMessage ? error.data.ExceptionMessage : 'Something went wrong, please try again ';
        var alertPopup = $ionicPopup.alert({
          title: 'Message',
          template: $scope.vm.error
        });
      });
    };


    $scope.ratting = function (sp) {
      $scope.showConfirm(sp);
    };

    $scope.showConfirm = function(sp) {

      var bidding = sp;
      var confirmPopup = $ionicPopup.confirm({
        title: 'Rate your service',
        template: 'Please rate by your exprence <br/> <div class="rating-sec"><input type="text" ng-model="vm.rating.comments"></rating></div><div class="rating-sec"><rating ng-model="vm.rating.rate" max="vm.rating.max"></rating></div>',
        scope: $scope
      });
      confirmPopup.then(function(res) {
        if(res) {

          var request= {
            "Id": 0,
            "ForUserId": sp.SPId,
            "FromUserId": $scope.vm.user.Id,
            "Rating": $scope.vm.rating.rate,
            "Comments": $scope.vm.rating.comments,
            "CreatedDate": new Date(),
            "RequestId": sp.Id
          };

          var msg = $scope.vm.user.FirstName +'provided rating'+ $scope.vm.rating.rate;
          RequestSrv.postRating(request, msg).then(function (res) {
            $scope.selectBidder(bidding);
          });

        } else {
          UserSrv.hideSpinner();
          $scope.vm.rating= {rate :0,max :5,comments:''};
        }
      });
    };
    
  }
} )();
