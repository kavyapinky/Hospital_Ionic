(function(){
  'use strict';
  angular.module('app')
    .controller('ServiceRequestCompleteCtrl', ServiceRequestCompleteCtrl);

    function ServiceRequestCompleteCtrl($scope, $state,$ionicHistory,UserSrv){
    	var vm = {};
    	$scope.vm = vm;
        UserSrv.hideSpinner();
        $ionicHistory.nextViewOptions({
            disableBack: true
          });
    	$scope.serviceCompleteMessage = 'Your request has processed successfully. Kindly wait for sometime so that we allocate the provider.';

    	$scope.goToStartNewRequest = function () {
    		$state.go('app.category');
    	};

    	$scope.goToDashboard = function () {
    		$state.go('app.category');
    	};
    }

})();
