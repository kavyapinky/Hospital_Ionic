(function(){
  'use strict';
  angular.module('app')
  .controller('SelectVendorCtrl', SelectVendorCtrl);

  function SelectVendorCtrl($scope, $state, $http ,$ionicPopup, StorageUtils, UserSrv, RequestSrv,$ionicHistory){
    var vm = {};
    $scope.vm = vm;
    UserSrv.hideSpinner();
    var request = {};
    $scope.request= {};
    $scope.VendorsList= {};

    $scope.$on('$ionicView.enter', function(viewInfo){
      getUser();
    });

    function getUser(){
      UserSrv.get().then(function(user){
        vm.user = user;
      });

      StorageUtils.get('newRequest').then(function(res){
        request = res
        $scope.selectedCategory = request.category;
        $scope.selectedSubCategory = request.subCategory;
        $scope.request.category = $scope.selectedCategory;
        $scope.request.subCategory = $scope.selectedSubCategory;
        $scope.request= res.request || {};
      });
      RequestSrv.getVendors().then(function(res){
        $scope.VendorsList= res.data;
      },
      function(){

      })
    };

    $scope.goToProcessComplete = function (vendor) {
    
      $scope.request.SPId = vendor.UserId;
      request.request= $scope.request;
      StorageUtils.set('newRequest', request)
      $state.go('app.summary');

    };


  }
})();
