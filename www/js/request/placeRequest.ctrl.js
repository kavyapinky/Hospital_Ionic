(function(){
  'use strict';
  angular.module('app')
  .controller('PlaceRequestCtrl', PlaceRequestCtrl);

  function PlaceRequestCtrl($scope, $state, $http ,StorageUtils, UserSrv, RequestSrv,AuthSrv,$ionicPopup,$q){
    var vm = {};
    $scope.vm = vm;
    UserSrv.hideSpinner();
    $scope.request = {};
    vm.index = 0;
    var request = {};
    $scope.hours = [];

    $scope.options = {
      componentRestrictions:{'country':'IN'}
    }

    $scope.$on('$ionicView.enter', function(viewInfo){
      getUser();

      getLocation().then(function(address){

        address = $scope.request.address || address
        var input =document.getElementById("gaddress");
        var ngModel = angular.element(input).controller('ngModel');
        ngModel.$setViewValue(address);
        input.value = address.formatted_address;

      });


    });


    function getLocation() {
        return $q(function (resolve, reject) {
            navigator.geolocation.getCurrentPosition(function (location) {
                var latlng = {
                  lat: location.coords.latitude,
                  lng: location.coords.longitude
                };
                var geocoder = new google.maps.Geocoder();
                geocoder.geocode({'location': latlng}, function (results, status) {
                  if (status == google.maps.GeocoderStatus.OK) {
                    if (results[1]) {
                      resolve(results[1]);
                    } else {
                      resolve(results[0])
                    }
                  } else {
                    var error = {
                      status: status,
                      from: 'reverseGeocoding'
                    };
                    reject(error);
                  }
                })
            }, function (error) {
                error.from = 'getLocation';
                reject(error);
            });
        });
    };



    function getUser(){
      UserSrv.get().then(function(user){
        vm.user = user;
      });

      StorageUtils.get('newRequest').then(function(res){
        request = res
        $scope.selectedCategory = request.category;
        $scope.selectedSubCategory = request.subCategory;
        $scope.request.category = $scope.selectedCategory;
        $scope.request.subCategory = $scope.selectedSubCategory;
        $scope.request= res.request || {};
      });
    };

    $scope.datepickerObject = {
      templateType: 'popup', 
      showTodayButton: 'true',
      from: new Date(),
      closeOnSelect: 'true',
      callback: function (val) {  
        if (val) {
          $scope.request.expectedDate= val;
          $scope.hours  = $scope.getHours(val); 
        };
      }
    };

    $scope.getHours = function(date){
      var hr = [];
      hr.push({id: 1, name: '8 AM to 11 AM'});
      hr.push({id: 2, name: '11 AM to 02 PM'});
      hr.push({id: 3, name: '02 PM to 05PM'});
      hr.push({id: 4, name: '05 PM to 09 PM'});
      return hr;
    };

    $scope.openTimeSlot = function(date){
      if($scope.hours.length == 0)
        return;
        $ionicPopup.show({
          templateUrl: 'js/request/timeSlotTemplate.html',
          title: 'Delivery Time',
          subTitle: '',
          scope: $scope,
          buttons: [
            {
              text: 'Close',
              type: 'button-stable',
              onTap: function (e) {
                
              }
            },
            {
              text: 'Set',
              type: 'button-positive',
              onTap: function (e) {

                $scope.request.hour= $scope.hours[vm.index];
              }
            }
          ]
        });
    }


    $scope.goToProcessComplete = function () {
      if (AuthSrv.isLogged()) {
        
        request.request= $scope.request;
        StorageUtils.set('newRequest', request)
        $state.go('app.selectVendors');
      }
      else 
      {
        var alertPopup = $ionicPopup.alert({
             title: 'Message',
             template: 'Please login to avail this services'
           });
        alertPopup.then(function() {
            $state.go('login');
        });
        
      }
    };
  }
})();
