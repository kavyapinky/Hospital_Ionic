(function(){
  'use strict';
  angular.module('app')
  .controller('SummaryCtrl', SummaryCtrl);

  function SummaryCtrl($scope, $state, $http ,$ionicPopup, StorageUtils, UserSrv, RequestSrv,$ionicHistory){
    var vm = {};
    $scope.vm = vm;
    UserSrv.hideSpinner();
    var request = {};
    $scope.request= {};

    $scope.$on('$ionicView.enter', function(viewInfo){
      getUser();
      $ionicHistory.nextViewOptions({
        disableAnimate: true,
        disableBack: true
      });
    });

    function getUser(){
      UserSrv.get().then(function(user){
        vm.user = user;
      });

      StorageUtils.get('newRequest').then(function(res){
        request = res
        $scope.selectedCategory = request.category;
        $scope.selectedSubCategory = request.subCategory;
        $scope.request.category = $scope.selectedCategory;
        $scope.request.subCategory = $scope.selectedSubCategory;
        $scope.request= res.request || {};
      });
    };

    $scope.goToProcessComplete = function () {
      var user =vm.user;
      UserSrv.showSpinner();
      $scope.address = {};
      if($scope.request.address)
      {
        $scope.address.address = $scope.request.address.formatted_address;
        $scope.address.lat = $scope.request.address.geometry.location.lat();
        $scope.address.lng = $scope.request.address.geometry.location.lng();
      }

      $scope.address.buildingName = $scope.request.buildingName;
      $scope.address.landmark = $scope.request.landmark;
      $scope.address.pincode = $scope.request.pincode;

      var requestObj = {
        "Id": 0,
        "CustomerId": user.Id,
        "SPId": $scope.request.SPId,
        "CategoryId":  request.subCategory.Id,
        "BiddingDetails": JSON.stringify([]),
        "ChatDetails": JSON.stringify([]),
        "Description": $scope.request.description ||'',
        "DeliveryDate": $scope.request.expectedDate,
        "DeliveryTime": $scope.request.hour.name ,
        "BefoureDate": $scope.request.befoureDate || new Date(),
        "Address": JSON.stringify($scope.address),
        "PhoneNo": $scope.request.phoneNo,
        "Status": 'open',
        "CreatedOn": new Date(),
        "ExpiryDate": new Date()
      }

      var msg = "New request is placed by "+ user.UserName+" for " +$scope.selectedSubCategory.CategoryName;

      RequestSrv.postRequest(requestObj).then(function(res){
        UserSrv.hideSpinner();
        StorageUtils.remove('newRequest');
        $state.go('app.requestComplete');
      },function(error){

        UserSrv.hideSpinner();
        vm.error = error.data && error.data.ExceptionMessage ? error.data.ExceptionMessage : "Something went wrong, Please check all credentials and try again";
        var alertPopup = $ionicPopup.alert({
          title: 'Warning',
          template: vm.error 
        });

      });

    };


  }
})();
