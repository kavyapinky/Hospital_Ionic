(function(){
  'use strict';
  angular.module('app')
    .controller('DashboardCtrl', DashboardCtrl);

  function DashboardCtrl($scope, $state){
    var vm = {};
    $scope.vm = vm;
    UserSrv.hideSpinner();

    $scope.goToNewRequestScreen = function () {
        $state.go('app.category');
    };

    $scope.goToMyRequestScreen = function () {
        $state.go('app.myRequestTab');
    };

  }
})();
