(function(){
  'use strict';
  angular.module('app')
    .config(configure);

  function configure($stateProvider){
    $stateProvider
    .state('app.dashboard', {
      url: '/dashboard',
      views: {
        'menuContent': {
          templateUrl: 'js/dashboard/dashboard.html',
          controller: 'DashboardCtrl'
        }
      }
    });
  }
})();
