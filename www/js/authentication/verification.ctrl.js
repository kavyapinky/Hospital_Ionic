(function(){
	'use strict';
	angular.module('app')
	.controller('verificationCtrl', verificationCtrl);

	function verificationCtrl($scope, $state, $ionicPopup,AuthSrv,UserSrv,PushPlugin,ToastPlugin){
		var vm = {};
		$scope.vm = vm;
		vm.error = '';
		vm.verifyEmail = verifyEmail;
		vm.message = '';
		vm.resendVirificationCode = resendVirificationCode;
		UserSrv.hideSpinner();

		$scope.$on('$ionicView.enter', function(viewInfo){
			getUser();
		});

		function getUser(){
			return UserSrv.get().then(function(user){
				vm.user = user;
			});
		};

		function resendVirificationCode () {
			UserSrv.showSpinner();
			AuthSrv.resendVirificationCode(vm.user.Id).then(function(response){
				UserSrv.hideSpinner();
					if (response.data) {
						var alertPopup = $ionicPopup.alert({
						 title: 'Success',
						 template: 'We have sent new verification code'
					 });
				}
					else {
						vm.error = 'oops we can\'t send mail please try again later.'
					}
					
			}, function(error){
				UserSrv.hideSpinner();
				vm.error = error.data && error.data.ExceptionMessage ? error.data.ExceptionMessage : "Something went wrong, Please check all credentials and try again";
				var alertPopup = $ionicPopup.alert({
					title: 'Warning',
					template: vm.error 
				});
				
			});
		}

		function verifyEmail(){
			

			
			UserSrv.showSpinner();
			if(vm.code == vm.user.otp){

				AuthSrv.verifiyOtp({ "userId": vm.user.userId}).then(function(response){
					UserSrv.hideSpinner();
					if (response.Success) {
						vm.user.logged = true;
						UserSrv.set(vm.user);
						$state.go('app.category');
					}
					
				}, function(error){
					UserSrv.hideSpinner();
					vm.error = error.Message || "Something went wrong, Please check all credentials and try again";
					var alertPopup = $ionicPopup.alert({
						title: 'Warning',
						template: vm.error 
					});
				});
			}
			else{
				UserSrv.hideSpinner();
				vm.error = 'Please Provide valid otp'
				var alertPopup = $ionicPopup.alert({
					title: 'Error',
					template: 'Please enter valid verification code'
				});
			}

		}
		
	}
})();