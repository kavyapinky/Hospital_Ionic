(function(){
  'use strict';
  angular.module('app')
    .config(configure);

  function configure($stateProvider){
    $stateProvider
      .state('login', {
      url: '/login',
      templateUrl: 'js/authentication/login.html',
      controller: 'LoginCtrl',
      data: {
        restrictAccess: ['notLogged']
      }
    })

    .state('forgot', {
      url: '/forgot',
      templateUrl: 'js/authentication/forgot.html',
      controller: 'ForgotCtrl',
      data: {
        restrictAccess: ['notLogged']
      }
    })

    .state('signup', {
      url: '/signup',
      templateUrl: 'js/authentication/signup.html',
      controller: 'SignupCtrl',
      data: {
        restrictAccess: ['notLogged']
      }
    })

    .state('verification', {
      url: '/verification',
      templateUrl: 'js/authentication/verification.html',
      controller: 'verificationCtrl'
    })

    .state('change-password', {
      url: '/change-password',
      templateUrl: 'js/authentication/change-password.html',
      controller: 'ChangePasswordCtrl',
      data: {
        restrictAccess: ['notLogged']
      }
    })
    
    .state('app.change-password', {
      url: '/change-password',
      views: {
        'menuContent': {
          templateUrl: 'js/authentication/change-password.html',
          controller: 'ChangePasswordCtrl'
        }
      }
    })



  }
})();
