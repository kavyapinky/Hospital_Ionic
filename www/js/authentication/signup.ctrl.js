(function(){
  'use strict';
  angular.module('app')
  .controller('SignupCtrl', SignupCtrl);

  function SignupCtrl($scope, $state, $ionicPopup,AuthSrv,UserSrv,PushPlugin,ToastPlugin){
    var vm = {};
    $scope.vm = vm;
    $scope.customer = {};
    UserSrv.hideSpinner();
    vm.goToLandingScreen = goToLandingScreen;
    $scope.$on('$ionicView.enter', function(viewInfo){
    });

    function goToLandingScreen(customer){
      vm.error = '';
     
      var pushId = '';
      UserSrv.get().then(function(user){
        pushId = user.pushId

        var req= {
          "fullName":$scope.customer.name,
          "emailId": $scope.customer.email,
          "password": $scope.customer.password,
          "phoneNumber": $scope.customer.phoneNumber,
          "loginType": 1,//1 for email, 2 for facebook, 3 for googlePlus
          "deviceType": 2,//1 for Iphone, 2 for android
          "deviceToken": pushId || 0,
          "userType": 2,//2 for Customer, 3 for provider
          "isLogin": false
        }
        
        UserSrv.showSpinner();
        AuthSrv.createUser(req).then(function(response){
          
          UserSrv.hideSpinner();
          if (response.data == 0) {
              $scope.isUserAlreadyPresent=true;
              vm.error = ' Already User Present with userName or email'
              return;
          }

          UserSrv.set(response.Result);
          $state.go('verification');
          
        }, function(error){

          UserSrv.hideSpinner();
          vm.error = error.Message || "Something went wrong, Please check all credentials and try again";
          var alertPopup = $ionicPopup.alert({
             title: 'Message',
             template: vm.error 
           });
          
        });

      });

    };
  }
})();