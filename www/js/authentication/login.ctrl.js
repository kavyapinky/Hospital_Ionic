(function(){
  'use strict';
  angular.module('app')
    .controller('LoginCtrl', LoginCtrl);

  function LoginCtrl($scope, $state, AuthSrv,UserSrv,$ionicPopup){
    var vm = {};
    $scope.vm = vm;
    UserSrv.hideSpinner();
    vm.error = null;
    vm.loding = false;
    vm.credentials = {login: '', password: ''};
    vm.login = login;
    vm.googleLogin= googleLogin;
    vm.faceBookLogin = faceBookLogin;
    vm.skipLogin = skipLogin;
    var pushId ='';



    $scope.$on('$ionicView.enter', function(viewInfo){

      UserSrv.get().then(function(user){
        pushId = user.pushId
      });

    });


    function login(credentials){
      vm.error = null;
      UserSrv.showSpinner();
      credentials.pushId = pushId;

       var req= {
        "emailId": credentials.userName,
        "password": credentials.password,
        "loginType": 1,//1 for email, 2 for facebook, 3 for googlePlus
        "deviceType": 2,//1 for Iphone, 2 for android
        "deviceToken": credentials.pushId,
        "userType": 2,//2 for Customer, 3 for provider
        "profilePicture": "string",
        "isLogin": true
      }

      AuthSrv.login(req).then(function(response){
        AuthSrv.isSkip = false;

        if(!response.isOTPVerify){
          $state.go('verification');
          return;
        }
        else
        {
          response.logged = true;
          UserSrv.set(response);
          $state.go('app.category');
        }

        vm.credentials.password = '';
        vm.error = null;
        UserSrv.hideSpinner();
      },function(error){
        vm.credentials.password = '';
        printError(error.Message);
      });

    };

    function skipLogin(){
      AuthSrv.isSkip = true;
       $state.go('app.category');
    };

    function googleLogin(credentials){
      credentials.pushId = pushId;
      vm.error = null;
      credentials.pushId = pushId;
      AuthSrv.googleLogin(credentials).then(function(response){
        AuthSrv.isSkip = false; 
        $state.go('app.category');      
      }, function(error){
          printError(error.Message);
      });
    };

    function faceBookLogin(credentials){
      vm.error = null;
      credentials.pushId = pushId;
      AuthSrv.faceBookLogin($scope.RoleId,pushId).then(function(response){
        AuthSrv.isSkip = false;
        $state.go('app.category');
      }, function(error){
          printError(error.Message);
      });
    };

    
  function printError(mes){
      UserSrv.hideSpinner();
      vm.error = mes || "Something went wrong, Please check all credentials and try again";
      var alertPopup = $ionicPopup.alert({
       title: 'Message',
       template: vm.error
      });
  }

    
  }
})();
