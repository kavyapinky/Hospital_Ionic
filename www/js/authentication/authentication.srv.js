(function(){
  'use strict';
  angular.module('app')
    .factory('AuthSrv', AuthSrv)
    .factory('AuthInterceptor', AuthInterceptor);

  AuthSrv.$inject = ['$http', 'UserSrv', 'StorageUtils','$q','$cordovaOauth','api'];

  function AuthSrv($http, UserSrv, StorageUtils,q,$cordovaOauth,api){
    var service = {
      login: login,
      forgot: forgot,
      changePassword:changePassword,
      createUser:createUser,
      logout: logout,
      isLogged: isLogged,
      googleLogin:googleLogin,
      faceBookLogin:faceBookLogin,
      isSkip : isSkip,
      customer : customer,
      verifiyOtp : verifiyOtp,
      resendVirificationCode : resendVirificationCode
   
    };
    return service;
    var customer = {};
    var isSkip = false;

   
    
    function login(credentials){

      var deferred  = q.defer();
      api.users.signup.save(credentials).$promise.then(function(res){

        if(res.Success){
          var user = res.Result;
          user.logged = false;
         //StorageUtils.set('authToken', user.deviceToken);
          UserSrv.set(user).then(function(){
            deferred.resolve(user);
          });
        }
        else
        {
          deferred.reject(res);
        }
        
      },function (error) {
        deferred.reject(error);
      })

      return deferred.promise;

    }

    function googleLogin(credentials){

        return $cordovaOauth.google("65137484242-4i9koces16i2tj8d3s6rpdatj8gv9olu.apps.googleusercontent.com", 
          ["https://www.googleapis.com/auth/urlshortener", "email"]).then(function(result) {
            
            var user = result;
            user.logged = true;
            UserSrv.set(user);
            
            return result;
        }, function(error) {
            console.log(error);
            return error;
        });
    }

    function faceBookLogin(roleId,pushId){
       
       var deferred  = q.defer();

       $cordovaOauth.facebook("1530078253972283", ["email"]).then(function(result) {

        if(result.hasOwnProperty("access_token") === true) {

          $http.get("https://graph.facebook.com/v2.2/me", 
            { params: { 
                access_token: result.access_token, 
                fields: "id,name,email,gender,location,website,picture,relationship_status", 
                format: "json" 
              }
          })
          .then(function(result) {

            var user = result.data;
            var image = "";
            if(user && user.picture && user.picture.data){
              image = user.picture.data.url
            }

            var req= {
              "emailId": credentials.userName,
              "password": credentials.password,
              "loginType": 1,//1 for email, 2 for facebook, 3 for googlePlus
              "deviceType": 2,//1 for Iphone, 2 for android
              "deviceToken": credentials.pushId,
              "userType": 2,//2 for Customer, 3 for provider
              "profilePicture": image,
              "isLogin": true
            }

            return login(req);

          }, function(error) {
              deferred.reject(error);
          });
        }

      }, function(error) {
        deferred.reject(error);
      });
      
      return deferred.promise;
    }

    function forgot(credentials){

      return $http.get(Config.backendUrl + Config.apiKey.forgotPassword + '?userName='+credentials.userName).success(function (response) {
        return response
      })
      .error(function (response) {
        return response;
       });
    }

    function changePassword(credentials){

      return $http.post(Config.backendUrl + Config.apiKey.changePassword +'?userName='+credentials.UserName+'&newPassword='+credentials.password+'&OldPassword='+credentials.oldPassword).success(function (response) {
        return response;
      })
      .error(function (response) {
        return response;
      });
    }

    function verifiyOtp(user){
      
      return api.users.verifyOtp.save(user).$promise.then(function(res){
        return res;
      },function(error){
        return error;
      })
     
    }

    function resendVirificationCode(id, code){
      return $http.get(Config.backendUrl+ Config.apiKey.users + '/' + id + '/verification/mail?isSendMail=true').success(function (response) {
        return response;
      })
      .error(function (response) {
        return response;
      });
    }

    function logout(){
      var user ={};
      user.logged = false;
      StorageUtils.remove('newRequest');
      return UserSrv.set(user);
    }


   

    function createUser(user){
      var deferred  = q.defer();
      api.users.signup.save(user).$promise.then(function(res){
         deferred.resolve(res);
      },function (error) {
        deferred.reject(error);
      })
      return deferred.promise;

    }

   

    function isLogged(){
      var user = StorageUtils.getSync(UserSrv.storageKey);
      return user && user.logged === true;
    }

  }

  AuthInterceptor.$inject = ['$q', '$location', '$log','StorageUtils','$rootScope'];
  function AuthInterceptor($q, $location, $log,StorageUtils,$rootScope){
    var service = {
      request: onRequest,
      response: onResponse,
      responseError: onResponseError
    };
    return service;

    function onRequest(config){
      // add headers here if you want...
      
      var access_token = StorageUtils.getSync('authToken');
      if (access_token) {
          config.headers.Authorization = 'Bearer ' + access_token;
      }

      return config;

    }

    function onResponse(response){
      $rootScope.$broadcast('loading:hide')
      return response;
    }

    function onResponseError(response){
      $log.warn('request error', response);
      $rootScope.$broadcast('loading:hide')
      if(response.status === 401 || response.status === 403){
        // user is not authenticated
        StorageUtils.get('user').then(function(user){
          user.logged = false;
          StorageUtils.set('user',user).then(function(user){
            $location.path('/login');
          });
        })
        
      }
      return $q.reject(response);
    }
  }
})();
