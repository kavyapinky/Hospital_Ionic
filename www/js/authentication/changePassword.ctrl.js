(function(){
  'use strict';
  angular.module('app')
  .controller('ChangePasswordCtrl', ChangePasswordCtrl);

  function ChangePasswordCtrl($scope, $state, $ionicPopup,AuthSrv,UserSrv){
    var vm = {};
    $scope.vm = vm;
   
    


    vm.goToLandingScreen = goToLandingScreen;
    UserSrv.hideSpinner();

    $scope.$on('$ionicView.enter', function(viewInfo){
      getUser();
      $scope.customer = {};
    });

    function getUser(){
      return UserSrv.get().then(function(user){
        vm.user = user;
      });
    };



    function goToLandingScreen(credentials){
      UserSrv.showSpinner();
      credentials.UserName = vm.user.UserName
      AuthSrv.changePassword(credentials).then(function(response){
        if(response.data && response.data == true){
          var alertPopup = $ionicPopup.alert({
            title: 'Success',
            template: 'You have successfully changed your password '
          }).then(function(){
            $state.go('app.category');
          });

          UserSrv.hideSpinner();
        }
        else{
          UserSrv.hideSpinner();

          var alertPopup = $ionicPopup.alert({
            title: 'Message',
            template: 'please enter correct current password '
          });

        };
        UserSrv.hideSpinner();
      },function(error){

        UserSrv.hideSpinner();
        var alertPopup = $ionicPopup.alert({
          title: 'Message',
          template: error.ExceptionMessage || 'Something went wrong please try again later '
        });
      })
    };
  }
})();
