(function(){
  'use strict';
  angular.module('app')
    .controller('ForgotCtrl', ForgotCtrl);

  function ForgotCtrl($scope, $state, $ionicPopup,AuthSrv,UserSrv){
    var vm = {};
    $scope.vm = vm;
    $scope.customer = {};
    
    UserSrv.hideSpinner();
    vm.goToLandingScreen = goToLandingScreen;

    function goToLandingScreen(credentials){
      UserSrv.showSpinner();
      AuthSrv.forgot(credentials).then(function(response){
        var alertPopup = $ionicPopup.alert({
                 title: 'Forgot',
                 template: 'Temporary password sent to your mail address'
               });
               alertPopup.then(function(res) {
                 $state.go('login');
               });
        UserSrv.hideSpinner();
      }, function(error){
        UserSrv.hideSpinner();
        $scope.customer.userName = '';
        $scope.vm.error = error.data && error.data.ExceptionMessage ? error.data.ExceptionMessage : 'UserName is invalid, Please try again later.';
        var alertPopup = $ionicPopup.alert({
          title: 'Message',
          template: $scope.vm.error
        });
        
      });
    };
  }

})();
