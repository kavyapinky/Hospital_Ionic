(function(){
  'use strict';
  angular.module('app')
    .factory('UserSrv', UserSrv);

  UserSrv.$inject = ['StorageUtils','$ionicLoading'];
  function UserSrv(StorageUtils,$ionicLoading){
    var userKey = 'user';
    var service = {
      storageKey: userKey,
      get: getCurrentUser,
      set: setCurrentUser,
      delete: deleteCurrentUser,
      showSpinner:showSpinner,
      hideSpinner:hideSpinner
    };
    return service;

    function getCurrentUser(){
      return StorageUtils.get(userKey);
    }

    function setCurrentUser(user){
      return StorageUtils.set(userKey, user);
    }

    function deleteCurrentUser(){
      return StorageUtils.clear(userKey);
    }

    function showSpinner() {
      $ionicLoading.show({
        template: '<p>Loading...</p><ion-spinner></ion-spinner>'
      });
    };

    function hideSpinner(){
      $ionicLoading.hide();
    };

  }
})();
