(function(){
  'use strict';
  angular.module('app')
    .constant('Config', Config)
    .constant('_', _)
    .constant('moment', moment)
    .constant('DB_CONFIG', {
	    name: 'taalomat',
	    tables: [{
	        name: 'categories',
	        columns: [
	            {name: 'Id', type: 'integer primary key'},
	            {name: 'CategoryName', type: 'text'},
	            {name: 'CategoryDescription', type: 'text'},
	            {name: 'ParentId', type: 'integer'},
	            {name: 'IsActive', type: 'text'},
	            {name: 'CreatedOn', type: 'text'},
	            {name: 'LastUpdated', type: 'text'},
	            {name: 'CssClass', type: 'text'}
	        ]
	    },{
	        name: 'user_categories',
	        columns: [
	            {name: 'id', type: 'integer primary key'},
	            {name: 'userId', type: 'integer'},
	            {name: 'categoryIdCollection', type: 'text'},
	            {name: 'lastupdatedOn', type: 'text'}
	            
	        ]
	    },{
	        name: 'user',
	        columns: [
	            {name: 'Id', type: 'integer primary key'},
	            {name: 'RoleId', type: 'integer'},
	            {name: 'RegisterId', type: 'text'},
	            {name: 'LoginId', type: 'text'},
	            {name: 'UserName', type: 'text'},
	            {name: 'FirstName', type: 'text'},
	            {name: 'LastName', type: 'text'},
	            {name: 'Address1', type: 'text'},
	            {name: 'Address2', type: 'text'},
	            {name: 'Others', type: 'text'},
	            {name: 'City', type: 'text'},
	            {name: 'State', type: 'text'},
	            {name: 'Country', type: 'text'},
	            {name: 'PhoneNumber', type: 'text'},
	            {name: 'Image', type: 'text'}
	            
	        ]
	    },{
	        name: 'notification',
	        columns: [
	            {name: 'message', type: 'text'},
	            {name: 'date', type: 'text'}
	        ]
	    }]
	});
})();
