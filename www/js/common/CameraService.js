(function(){
  'use strict';
  angular.module('app')
    .factory('CameraService', CameraService);


    function CameraService($q, CameraPlugin,$window)
    {
      return {

          getPicture: function(index) {
            
            var q = $q.defer();
            var source;   

            function optionsForType(type) {

                switch (type) {
                    case 0:
                    source = $window.Camera.PictureSourceType.CAMERA;
                    break;
                    case 1:
                    source = $window.Camera.PictureSourceType.PHOTOLIBRARY;
                    break;
                }
                return {
                        destinationType: Camera.DestinationType.DATA_URL,
                        sourceType: source,
                        allowEdit: false,
                        encodingType: Camera.EncodingType.JPEG,
                        saveToPhotoAlbum: false
                    };
            }
              
              var options = optionsForType(index);

              CameraPlugin.getPicture(options).then(function (imageUrl) {
                  q.resolve("data:image/jpeg;base64," +imageUrl);
              });

              return q.promise;
          }
      }
    }

    
})();