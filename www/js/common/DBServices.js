//=================================================
// DB
// - init
// - query
// - fetchAll
// - fetch
//=================================================
(function(){
  'use strict';
  angular.module('app')
    .factory('DB', DB);
    function DB($rootScope, $q, DB_CONFIG)
    {
        var self = this;
        self.db = null;

        self.init = function() {

            if($rootScope.platform == "pc"){
                self.db = window.openDatabase(DB_CONFIG.name, '1.0', 'database', -1);
            }else{
                self.db = window.sqlitePlugin.openDatabase({name: DB_CONFIG.name, bgType: 1 });
            }

            /*
            var query = 'DROP TABLE emt';
            self.query(query);
            */

            angular.forEach(DB_CONFIG.tables, function(table) {
                var columns = [];

                angular.forEach(table.columns, function(column) {
                    columns.push(column.name + ' ' + column.type);
                });

                var query = ' CREATE TABLE IF NOT EXISTS ' + table.name + ' (' + columns.join(',') + ')';
                //var query = ' Drop TABLE ' + table.name + ' ';
                self.query(query);

                console.log('+ App: Table ' + table.name + ' initialized');
            });

        };

        self.cleanUp = function(){
            angular.forEach(DB_CONFIG.tables, function(table) {
                
                var query = ' DROP TABLE IF EXISTS ' + table.name ;
                self.query(query);
                console.log('+ App: Table ' + table.name + ' Droped');
            });

            self.init();
        }

        self.query = function(query, bindings) {
            bindings = typeof bindings !== 'undefined' ? bindings : [];
            var deferred = $q.defer();
            if(self.db && self.db.transaction){


                self.db.transaction(function(transaction) {
                    transaction.executeSql(query, bindings, function(transaction, result) {
                        deferred.resolve(result);
                    }, function(transaction, error) {
                        deferred.reject(error);
                    });
                });
            }
            else
            {
                deferred.reject('transaction is not defined');
            }

            return deferred.promise;
        };

        self.fetchAll = function(result) {
            var output = [];

            for (var i = 0; i < result.rows.length; i++) {
                output.push(result.rows.item(i));
            }

            return output;
        };

        self.fetch = function(result) {
            if(result.rows.length > 0)
                return result.rows.item(0);
            else
            {
                return {};
            }
        };

        return self;
    }
})();