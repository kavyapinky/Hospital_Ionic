(function () {
  'use strict'

   var api_routes = {

      'auth': ['auth',undefined, {
         'login': '/login'
      }],

      'users':['webapi/users',undefined,{
        'categories':'/getcategories',
        'signup' : '/signup',
        'verifyOtp' : '/verifyOtp',
        'organization': '/organization',
        'getprofile':'/getprofile/:userId'
      }],

      roles: ['roles', undefined, {
         'company' : ['/companies/:companyId/roles', undefined, {
            'roleId': '/:roleId'
         }],
         'roleId':  ['/:roleId', undefined, {
            'users': '/users'
         }],
         'id': '/:id'
      }],

     
   };

   angular.module('app')
   .constant('api_config', {

      //api_root: 'http://fnfapi.xcdify.com/',
      //api_root: 'http://192.168.1.6/',
      api_root: 'http://localhost:28287/',
      
      api_routes: api_routes

   });

})();