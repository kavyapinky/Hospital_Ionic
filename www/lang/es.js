
//=================================================================
// https://github.com/PascalPrecht/angular-translate-ng-newsletter-article/blob/master/article.md
// http://angular-translate.github.io/
// http://angular-translate.github.io/docs/#/api/pascalprecht.translate.filter:translate
//=================================================================
// {{'TITLE' | translate}}
// <button ng-click="changeLanguage('de')" translate="BUTTON_LANG_DE"></button>
// {{ 'WITH_VALUES' | translate:'{value: 5}' }} --> 'WITH_VALUES': 'The following value is dynamic: {{value}}'
//=================================================================
angular.module('app')
.config(function ($translateProvider) {

    $translateProvider.translations('es', {


        CONTINUE: 'जारी...',
        CUSTOMER: 'ग्राहक',
        LOGIN: 'लॉग इन करें',
        NEW_USER: 'नए उपयोगकर्ता',
        SAVE: 'बचाओ',
        OK: 'ठीक',
        DETAILS: 'विवरण',
        EDIT: 'संपादित करें',
        SEARCH: 'खोज',
        EDIT_PROFILE: 'प्रोफ़ाइल संपादित करें',
        CONNECT: 'कनेक्ट',
        CANCEL: 'रद्द',
        CONFIRM: 'पुष्टि',
        REGISTER: ' रजिस्टर करना',
        FROM: 'से',
        TO: 'को',
        DATE: 'तारीख',
        TYPE: 'टाइप',
        GOODS: 'माल',
        UPLOAD: 'अपलोड',
        NEXT: 'अगला',
        NEW_USER_REGISTERATION: 'नई उपयोगकर्ता registeration',

        NEW_REQUESTS: 'नए अनुरोधों',
        MY_PROFILE: 'मेरी प्रोफाइल',
        CALENDAR: 'कैलेंडर',
        LOCATION: 'स्थान',
        REQUEST: 'अनुरोध (ओं)',

        //profile
        Name: 'नाम',
        Phone_Number: 'फोन नंबर',
        Email: 'ईमेल',
        Address: 'पता',
        Taluk: 'तालुक',
        District: 'जिला',
        State: 'राज्य',
        Town: 'शहर',
        Pin_Code: 'पिन कोड',
        Area: 'क्षेत्र ',
        GET_STARTED : 'शुरू हो जाओ'
    });

});