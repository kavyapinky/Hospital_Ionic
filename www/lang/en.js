
//=================================================================
// https://github.com/PascalPrecht/angular-translate-ng-newsletter-article/blob/master/article.md
// http://angular-translate.github.io/
// http://angular-translate.github.io/docs/#/api/pascalprecht.translate.filter:translate
//=================================================================
// {{'TITLE' | translate}}
// <button ng-click="changeLanguage('de')" translate="BUTTON_LANG_DE"></button>
// {{ 'WITH_VALUES' | translate:'{value: 5}' }} --> 'WITH_VALUES': 'The following value is dynamic: {{value}}'
//=================================================================
  angular.module('app')
	.config(function ($translateProvider) {

	$translateProvider.translations('en', {

		// GENERALES
		//---------------------------------------------------
	    CONTINUE: 'CONTINUE',
		CUSTOMER: 'Customer Login',
		LOGIN: 'LOGIN',
		SAVE: 'SAVE',
		OK: 'OK',
		DETAILS: 'DETAILS',
		EDIT: 'EDIT',
		SEARCH: 'SEARCH',
		CONNECT: 'CONNECT',
		CANCEL: 'CANCEL',
		REJECT: 'REJECT',
		CONFIRM: 'CONFIRM',
		REGISTER: 'REGISTER',
		FROM: 'From',
		TO: 'To',
		DATE: 'Date',
		CAPACITY: 'Capacity',
		TYPE: 'Type',
		GOODS: 'Goods',
		UPLOAD: 'UPLOAD',
		NEXT: 'NEXT',
		

	    //profile
		Name: 'Name',
		Phone_Number: 'Phone Number',
		Email: 'Email',
		Address: 'Address',
		Taluk: 'Taluk',
		District: 'District',
		State: 'State',
		Town: 'Town',
		Pin_Code: 'Pin Code',
		Area: 'Area',

		GET_STARTED : 'GET STARTED'
		
	});

	$translateProvider.preferredLanguage('es');
});

